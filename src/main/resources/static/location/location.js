var getRole = localStorage.getItem("idRole")
var getUserId = localStorage.getItem("userId")


$('.navbar').attr('hidden', true)
$(function() {
	
	if (getRole != 1) {
		location.href = "/index";
	}

	$('#btn-masuk').off('click').html('Masuk').on('click', function() { modalLogin() })
	$('#btn-daftar').off('click').html('Daftar').on('click', function() { modalDaftar() })
	
	getAllLocation(0, 2, 0);

	$.ajax({
		url: '/api/levellokasi',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = '<option value=0>Level Lokasi </option>'

			for (var i = 0; i < data.length; i++) {
				str += '<option value = ' + data[i].id + ' >' + data[i].name + '</option>'
			}
			$('#levelLokasi').html(str)

		}
	})

})



var level = 0
function levelLokasi(lvl) {
	level = lvl
	getAllLocation(0, 2, level)
}

function getAllLocation(currentPage, length, level) {
	$.ajax({
		url: '/api/lokasi/pagging?page=' + currentPage + '&size=' + length + '&level=' + level,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			console.log(rawData)
			var data = rawData.variants
			var str = "<br>"
			str += "<table border='1' style='border-style: dotted; width: 90%; position: relative;left: 65px;' class='table table-borderless' style='align-self: center; width: 90%;'>";
			str += "<thead bgcolor='#08c0ff' style='color: white;'>";
			str += "<th width=25% class='text-center'>Nama</th>";
			str += "<th width=25% class='text-center'>Level Lokasi</th>"
			str += "<th width=25% class='text-center'>Wilayah</th>"
			str += "<th colspan='2' width=25% class='text-center' align='justify'>#</th>"
			str += "</thead>";
			str += "<tbody>";
			for (var i = 0; i < data.length; i++) {
				console.log(data[i])

				str += "<tr>";
				str += "<td class='text-center'>" + data[i].name + "</td>"

				if (data[i].parent != null) {
					str += "<td class='text-center'>" + data[i].mlocationLevel.name + "</td>"
					str += "<td class='text-center'>" + data[i].parent.mlocationLevel.abbreviation + " " + data[i].parent.name + "</td>"

				} else {
					str += "<td class='text-center'>" + data[i].mlocationLevel.name + "</td>"
					str += "<td class='text-center'>Level lokasi tertinggi</td>"
				}
				//str += "<td class='text-center'><div><button class='btn btn-warning' onclick='openModalEdit(" + data[i].id + ")'><i class='bi-pencil-square'></button></div></td>"
				//str += "<td class='text-center'><button class='btn btn-danger' onclick='openModalDelete(" + data[i].id + ")'><i class='bi-trash'></button></td>"
				str += "<td class='text-center'><i class='btn btn-warning bi-pencil-square' style='position: relative; left: 30%;' onclick='openModalEdit(" + data[i].id + ")'></td>"
				str += "<td class='text-center'><i class='btn btn-danger bi-trash' style='position: relative; right: 30%;' onclick='openModalDelete(" + data[i].id + ")'></td>"
				str += "</tr>";
			}
			str += "</tbody></table>";

			str += '<nav aria-label="Page navigation" style="position: relative; left: 68%">'
			str += '<ul class="pagination">'
			if (rawData.currentPage > 0) {
				str += '<li class="page-item"><a class="page-link" onclick="getAllLocation(' + (rawData.currentPage - 1) + ',' + length + ',' + level + ',' + level + ')"><<</a></li>'
			}
			else {
				str += '<li class="page-item"><a class="page-link"><<</a></li>'

			}
			var index = 1;
			for (i = 0; i < rawData.totalPage; i++) {
				str += '<li class="page-item"><a id="pageslink" class="page-link" onclick="getAllLocation(' + i + ',' + length + ',' + level + ')">' + index + '</a></li>'
				index++;
			}
			if (rawData.currentPage < rawData.totalPage - 1) {
				str += '<li class="page-item"><a class="page-link" onclick="getAllLocation(' + (rawData.currentPage + 1) + ',' + length + ',' + level + ')">>></a></li>'
			}
			else {
				str += '<li class="page-item"><a class="page-link">>></a></li>'
			}

			str += '</ul>'
			str += '</nav>'


			$("#isidata").html(str);
		}

	})
}

function searchLokasi(keyword) {
	var keyword = $('#search').val()
	console.log(keyword)
	if (keyword.length == 0) {
		getAllLocation(0, 2, 0);
	} else {
		$.ajax({

			url: '/api/lokasi/searching?keyword=' + keyword + '&level=' + level,
			type: 'get',
			contentType: 'application/json',
			success: function(data) {
				var str = "<br>"
				str += "<table border='1' style='border-style: dotted; width: 90%; position: relative;left: 65px;' class='table table-borderless' style='align-self: center; width: 90%;'>";
				str += "<thead bgcolor='#08c0ff' style='color: white;'>";
				str += "<th>Nama</th>";
				str += "<th>Level Lokasi</th>"
				str += "<th>Wilayah</th>"
				str += "<th align='justify'>#</th>"
				str += "</thead>";
				str += "<tbody>";
				for (var i = 0; i < data.length; i++) {

					str += "<tr>";
					str += "<td>" + data[i].name + "</td>"

					if (data[i].parent != null) {
						str += "<td>" + data[i].mlocationLevel.name + "</td>"
						str += "<td>" + data[i].parent.mlocationLevel.abbreviation + " " + data[i].parent.name + "</td>"

					} else {
						str += "<td>" + data[i].mlocationLevel.name + "</td>"
						str += "<td>Level lokasi tertinggi</td>"
					}
					str += "<td class='text-center'><i class='btn btn-warning bi-pencil-square' style='position: relative; left: 30%;' onclick='openModalEdit(" + data[i].id + ")'></td>"
					str += "<td class='text-center'><i class='btn btn-danger bi-trash' style='position: relative; right: 30%;' onclick='openModalDelete(" + data[i].id + ")'></td>"
					str += "</tr>";
				}
				str += "</tbody></table>";
				$("#isidata").html(str);
			}
		})
	}

}

function openModalTambah() {

	$.ajax({
		url: '/api/levellokasi',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = '<option value=0>--Pilih--</option>'

			for (var i = 0; i < data.length; i++) {
				str += '<option value = ' + data[i].id + ' >' + data[i].name + '</option>'
			}
			$('#locationLevelId').html(str)

		}
	})

	var teksTambah = "<div class='text-left'>"
	teksTambah += "<input type='hidden' id='locationId' value=0>"
	teksTambah += "<p style='margin-bottom: 0;'><small>Level Lokasi*</small></p>"
	teksTambah += "<select style='position: relative; width: 100%;' id='locationLevelId' onchange='locationByLocationLevelId(this.value)' required></select>"
	teksTambah += "<p id='validasiLokasi' class='text-danger'></p>"
	teksTambah += "<p style='margin-bottom: 0;'><small>Wilayah</small></p>"
	teksTambah += "<select style='position: relative; width: 100%;' id='parentId' onchange='cobaprint()' required><option>--Pilih--</option></select>"
	teksTambah += "<p style='margin-bottom: 0;'><br><small>Nama*</small></p>"
	teksTambah += "<input style='position: relative; width: 100%;' type='text' id='namaLokasi' required>"
	teksTambah += "<p id='validasiNamaTambah' class='text-danger'></p>"
	teksTambah += "</div>"

	$('.modal-title').html('Tambah Lokasi')
	$('.modal-body').html(teksTambah)
	$('.btn-primary').off('click').html('Simpan').on('click', function() { saveLocation($('#locationId').val()) })
	$('.modal-footer').css('justify-content', 'center')
	$('#modal').modal('show')

}

function locationByLocationLevelId(levelId, parentId) {
	var wil = levelId - 1
	$.ajax({
		url: '/api/lokasi/level/' + wil,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = '<option value=>Nama Wilayah</option>'

			for (var i = 0; i < data.length; i++) {
				if (data[i].id == parentId) {
					select = "selected";
				} else {
					select = "";
				}
				str += '<option value = ' + data[i].id + ' ' + select + '>' + data[i].name + '</option>'
			}
			$('#parentId').html(str)

		}
	})
}


function saveLocation(id) {
	//var id = $('#locationId').val()
	console.log(id)
	var locationLevelId = $('#locationLevelId').val()
	var parentId = $('#parentId').val()
	var name = $('#namaLokasi').val()
	console.log(locationLevelId)
	console.log(parentId)
	console.log(name)


	$.ajax({
		url: '/api/lokasi',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var val = 0 
			console.log("test " + name.length)
			if (name.length == 0) {
				alert("alert")
				$('#validasiNamaTambah').html('<small><i>*Mohon masukkan nama lokasi</i></small>')
				alert("alert2")
				val++
			}
			
			for (var i = 0; i < data.length; i++) {
				if (name.toLowerCase() == data[i].name.toLowerCase() && parentId == data[i].parentId) {
					//console.log(name.toLowerCase() + "==" + data[i].name.toLowerCase() + "&&"+ parentId + "==" + data[i].parentId)
					$('#validasiNama').html('<small><i>*Nama sudah ada untuk wilayah tersebut</i></small>')
					console.log("Nama sudah ada")
					val++
					break;
				} else {
					$('#validasiNama').html('')
					console.log("Nama belum ada")
				}
			}

			if (locationLevelId == 0) {
				$('#validasiLokasi').html('<small><i>*Level lokasi harus dipilih</i></small>')
				console.log("lokasi belum dipilih")
				val++
			} else {
				$('#validasiLokasi').html('')
				console.log("lokasi sudah dipilih")
			}

			console.log(val)

			if (val == 0) {
				var dataSave = '{'
				dataSave += '"locationLevelId" : "' + locationLevelId + '",'
				dataSave += '"parentId" : "' + parentId + '",'
				dataSave += '"name" : "' + name + '"'
				dataSave += '}'

				var link = ''
				var metode = ''
				if (id == 0) {
					link = '/api/lokasi/add'
					metode = 'post'
				} else {
					link = '/api/edit/lokasi/' + id + ''
					metode = 'put'
				}
				$.ajax({
					url: link,
					type: metode,
					contentType: 'application/json',
					data: dataSave,
					success: function() {
						$('#modal').modal('toggle')
						getAllLocation(0, 2, 0)
					}
				})
			}

		}
	})



}

function cobaprint() {
	console.log($('#locationLevelId').val())
	console.log($('#parentId').val())
}

function openModalEdit(id2) {
	openModalTambah()
	console.log(id2)
	$.ajax({
		url: '/api/lokasi/' + id2,
		type: 'get',
		contentType: 'application/json',
		success: function(data2) {

			$('#locationLevelId').val(data2.locationLevelId);
			locationByLocationLevelId(data2.locationLevelId, data2.parentId); //locationByLocationLevelId(levelId, parentId)
			$('#namaLokasi').val(data2.name);
			$('.modal-title').html('Edit');
			$('.btn-primary').off('click').html('Edit').on('click', function() { saveLocation(id2) })

		}
	})
}

function openModalDelete(id3) {
	console.log(id3)
	$.ajax({
		url: '/api/lokasi/' + id3,
		type: 'get',
		contentType: 'application/json',
		success: function(data2) {


			var teksHapus = "<h5 class='text-center'>Anda yakin akan menghapus lokasi '" + data2.name + "'?</h5>"

			$('.modal-body').html(teksHapus)
			$('.modal-title').html('Delete');
			$('.modal-footer').css('justify-content', 'center')
			$('#modal').modal('show')
			$('.btn-primary').off('click').html('Delete').on('click', function() { deleteLocation(id3) }).attr('hidden', false)

		}
	})
}

function deleteLocation(id) {
	//var id = $('#locationId').val()

	var teksHapus = '{ "deletedBy" : ' + getUserId + ' }';

	$.ajax({
		url: '/api/delete/lokasi/' + id + '',
		type: 'put',
		contentType: 'application/json',
		data: teksHapus,
		success: function(data) {
			$('.modal-body').html("<h5 class='text-center'>" + data + "</h5>")
			$('.modal-title').html('Delete');
			$('.btn-primary').attr('hidden', true)
		}
	})
}







