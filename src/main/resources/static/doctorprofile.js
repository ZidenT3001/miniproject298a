var getRole = localStorage.getItem("idRole")
var spesialisasiada = false
var getBiodataId = localStorage.getItem("biodataId")
var doctorId = 0
var userLogin = localStorage.getItem("userId")
var idCurrentSpecialization = 0

$(function() {
	
	if (getRole == null) {
		location.href = "/index";
	} else if (getRole != 3) {
		location.href = "/profile"
	}
	$('#btn-masuk').off('click').html('Keluar').on('click', function() { logout() })
	$('#btn-daftar').off('click').html('Profile').on('click', function() { location.href = "/profile" }).attr("hidden", true)

	$.ajax({
		url: '/api/mdoctor/biodata/' + getBiodataId,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			doctorId = data[0].id
			console.log(doctorId)

			getCurrentSpecialization()
		}
	})
})

function getCurrentSpecialization() {
	$.ajax({
		url: '/api/tcurrentdoctorspecialization/doctor/' + getBiodataId,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			console.log(data)

			if (data.length == 0) {
				$('#dokter-spesialisasi').html("Belum menambahkan spesialisasi")
				spesialisasiada = false
			} else {
				var namaSpesialisasiSekarang = data[0].mSpecialization.name
				$('#dokter-spesialisasi').html(namaSpesialisasiSekarang)
				spesialisasiada = true

				idCurrentSpecialization = data[0].id
			}


		}
	})
}

function exampleModal() {

	$.ajax({
		url: '/api/mspecialization',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			$('modal').modal('toggle')
			var str = '<option value=0>--Pilih--</option>'

			for (var i = 0; i < data.length; i++) {
				str += '<option value = "' + data[i].id + '" >' + data[i].name + '</option>'

			}

			$('#modal-title').html('spesialisasi dokter')
			$('#pilihSpesialisasi').html(str)
			$('#buttonSave').off('click').html('Simpan').on('click', function() { berhasilTambah() })
		}
	})


	var str = "<p style='margin-bottom: 0;'><small>Spesialisasi*</small></p>"
	str += "<select style='position: relative; width: 100%;' id='pilihSpesialisasi' onchange='' required></select>"

	$('.modal-title').html('Pilih Spesialisasi')
	$('.modal-body').html(str)
	$('#modal').modal('show')
}

function TambahSpesialisasi() {
	var formdata = '{'
	formdata += '"doctorId":' + doctorId + ','
	formdata += '"specializationId":' + $('#pilihSpesialisasi').val()
	formdata += '}'

	/*console.log($('#pilihSpesialisasi'))
	console.log(formdata)
	*/

	$.ajax({
		url: '/api/add/tcurrentdoctorspecialization/' + userLogin,
		type: 'post',
		data: formdata,
		contentType: 'application/json',
		success: function() {
			getCurrentSpecialization()
		}
	})
}

function ubahSpesialisasi() {
	var formdata = '{'
	formdata += '"doctorId":' + doctorId + ','
	formdata += '"specializationId":' + $('#pilihSpesialisasi').val()
	formdata += '}'

	console.log($('#pilihSpesialisasi'))
	console.log(formdata)


	$.ajax({
		url: '/api/edit/tcurrentdoctorspecialization/' + userLogin + '/' + idCurrentSpecialization,
		type: 'put',
		data: formdata,
		contentType: 'application/json',
		success: function() {
			getCurrentSpecialization()
		}
	})
}

function berhasilTambah() {

	//'$('#dokter-spesialisasi').html($('#pilihSpesialisasi').val())
	var str = ""

	if (!spesialisasiada) {
		str = "<p style='margin-bottom: 0;'><h3>Spesialisasi berhasil ditambahkan</h3></p>"
		TambahSpesialisasi()
	} else {
		str = "<p style='margin-bottom: 0;'><h3>Spesialisasi berhasil diubah</h3></p>"
		ubahSpesialisasi()
	}


	$('.modal-title').html('Sukses')
	$('.modal-body').html(str)
	$('#modal').modal('show')
}

/* function getTDoctorOfficeTreatmentByDoctorId(id) {
	$.ajax({
		url: "/api/tdoctorofficetreatment/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)

			if (data != null || data.length > 0) {

				var tindakan = ""

				var arrayTindakan = []

				for (var i = 0; i < data.length; i++) {
					tindakan = data[i].tDoctorTreatment.name
				}


				// hapus jika ada tindakan yang sama
				var uniqueWord = [];
				arrayTindakan.forEach((element) => {
					if (!uniqueWord.includes(element)) {
						uniqueWord.push(element);
					}
				});
				//console.log(uniqueWord)
				tindakan = ""
				uniqueWord.forEach((element) => {
					tindakan += "- " + element
					tindakan += "<br>"
				});


				// tampilkan data tindakan
				$("#pills-tindakan").html("")
				$("#accordion-body-tindakan").html(tindakan)
			}

		}
	})
}*/

function logout() {
	localStorage.clear();
	location.href = "/indexlogin";
}