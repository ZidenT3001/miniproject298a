var getRole = localStorage.getItem("idRole")

$(function() {
	if (getRole == null) {
		//getAllLandingPage()
		location.href = "/index";
	}
	$('#btn-masuk').off('click').html('Keluar').on('click', function() { logout() })
	$('#btn-daftar').off('click').html('Profile').on('click', function() { location.href="/profile" })
})

function getAllLandingPage() {

	var str = "<div class='dropdown'>"
	str += "<a href='#' class='nav-link dropdown-toggle btn btn-info' data-toggle='dropdown' aria-expanded='false'>"
	str += "<span class='d-none d-lg-inline-flex'>" + userLogin.nameUser + "</span>"
	str += "</a>"
	str += "<div class='dropdown-menu bg-light border-0 rounded-0 rounded-bottom m-0'>"

	if (userLogin.roleUser == "ROLE_DOKTER") {
		str += "<a href='/doctorprofilelayout' class='dropdown-item'>My Profile</a>"
	} else {
		str += "<a href='/profilelayout' class='dropdown-item'>My Profile</a>" 
	}
	str += "<a href='#' onclick='logout()' class='dropdown-item'>Log Out</a>"
	str += "</div>"
	str += "</div>"

	$('#btn-daftar').remove('')
	$('#btn-masuk').remove('')

}

function logout() {
	localStorage.clear();
	location.href = "/index";
}			
