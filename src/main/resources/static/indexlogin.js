var getRole = localStorage.getItem("idRole")

$(function() {
	if (getRole != null) {
		location.href = "/landingpage";
	}

	$('#btn-masuk').off('click').html('Masuk').on('click', function() { modalLogin() })
	$('#btn-daftar').off('click').html('Daftar').on('click', function() { modalDaftar() })
})

var mail
var pass
var tipe
var otpGen
var otpExp1
var otpExp2
var otpExp3
var otpExp4
var otpExp5
var x

function modalLogin() {

	var teksLogin = "<div class='text-left'>"
	teksLogin += "<p style='margin-bottom: 0;'><small>E-mail*</small></p>"
	teksLogin += "<input style='position: relative; width: 100%;' type='email' id='email' placeholder='varlkyrie123@mail.com' required>"
	teksLogin += "<p id='validasiEmail' class='text-danger'></p>"
	teksLogin += "<p style='margin-bottom: 0;'><small>Password*</small></p>"
	teksLogin += "<div class='input-group mb-3'>"
	teksLogin += "<input type='password' id='password' style='position: relative; width: 91%;' required>"
	teksLogin += "<div class='input-group-append'>"
	teksLogin += "<span class='input-group-text bi bi-eye-slash' value = '0' id='eyeId' onclick='togglePassword()'></span>"
	teksLogin += "</div>"
	teksLogin += "</div>"
	teksLogin += "<p id='validasiPassword' class='text-danger'></p>"
	teksLogin += "</div>"
	teksLogin += "<p style='position: relative; text-align: center;'><a href='#' onclick='openModalLupaPassword()'>Lupa Password?</a></p>"
	teksLogin += "<p style='position: relative; text-align: center;'><small>atau</small></p>"
	teksLogin += "<p style='position: relative; text-align: center;'><a href='#' onclick='modalDaftar()'>Belum punya akun? Daftar</a></p>"

	$('.modal-title').html('Masuk')
	$('.modal-body').html(teksLogin)
	$('.btn-secondary').attr('hidden', true)
	$('#buttonSave').off('click').html('Masuk').on('click', function() { login() })
	$('.modal-footer').css('justify-content', 'center')
	$('#modal').modal('show')
}

function togglePassword() {
	if ($('#eyeId').val() == 0) {
		$('#eyeId').val(1)
		$('#password').attr('type', 'text')
		$('#eyeId').attr('class', 'input-group-text bi bi-eye')
	} else {
		$('#eyeId').val(0)
		$('#password').attr('type', 'password')
		$('#eyeId').attr('class', 'input-group-text bi bi-eye-slash')
	}
}

function togglePassword2() {
	if ($('#eyeId2').val() == 0) {
		$('#eyeId2').val(1)
		$('#password2').attr('type', 'text')
		$('#eyeId2').attr('class', 'input-group-text bi bi-eye')
	} else {
		$('#eyeId2').val(0)
		$('#password2').attr('type', 'password')
		$('#eyeId2').attr('class', 'input-group-text bi bi-eye-slash')
	}
}

function login() {

	var salah = 0;
	var email = $('#email').val();
	var password = $('#password').val()
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
	

	if (email.length == 0) {
		$('#validasiEmail').html('<small><i>*Harap masukkan email</i></small>')
		salah++;
	} else if (!pattern.test(email)) {
		$('#validasiEmail').html("<small><i>*Format email salah</i></small>")
		salah++;
	} else { $('#validasiEmail').html('') }
	if (password.length == 0) {
		$('#validasiPassword').html('<small><i>*Harap masukkan password</i></small>')
		salah++;
	} else { $('#validasiPassword').html('') }


	if (salah == 0) {

		var formLogin = '{'
		formLogin += '"email":"' + $('#email').val() + '",'
		formLogin += '"password":"' + $('#password').val() + '"'
		formLogin += '}'

		$.ajax({
			url: '/api/userlogin',
			type: 'put',
			contentType: 'application/json',
			data: formLogin,
			success: function(data) {
				if (data == "emailsalah") {
					$('#validasiEmail').html('<small><i>*invalid username and password</i></small>')
					$('#validasiPassword').html('')
				} else if (data == "passwordsalah") {
					$('#validasiEmail').html('<small><i>*invalid username and password</i></small>')
					$('#validasiPassword').html('')
				} else {
					var emailLogged = data.email

					$.ajax({
						url: '/api/emailsearch/' + emailLogged,
						type: 'get',
						contentType: 'application/json',
						success: function(data) {
							console.log(data)
							localStorage.clear();
							localStorage.setItem("userId", data.id);
							localStorage.setItem("fullName", data.mbiodata.fullName);
							localStorage.setItem("biodataId", data.biodataId);
							localStorage.setItem("idRole", data.roleId);
							localStorage.setItem("roleCode", data.mrole.code);
							console.log("selesai, coba lihat localstorage")
							location.href = "/landingpage";
						}

					})
				}
			}
		})
	}
}

function modalDaftar() {
	tipe = 'Registration'

	var teksRegistrasi = "<div class='text-left'>"
	teksRegistrasi += "<p>Masukkan email Anda. Kami akan melakukan pengecekan</p>"
	teksRegistrasi += "<p style='margin-bottom: 0;'><small>Email*</small></p>"
	teksRegistrasi += "<input style='position: relative; width: 100%;' type='email' id='email' placeholder='varlkyrie123@mail.com' required>"
	teksRegistrasi += "<p id='validasiEmail' class='text-danger'></p>"
	teksRegistrasi += "</div>"


	$('.modal-title').html('Daftar')
	$('.modal-body').html(teksRegistrasi)
	$('.btn-secondary').attr('hidden', true)
	$('#buttonSave').off('click').html('Kirim OTP').on('click', function() { modalSetEmail($('#email').val()) })
	$('.modal-footer').css('justify-content', 'center')
	$('#modal').modal('show')

}

function modalSetEmail(email) {
	var salah = 0
	var email = $('#email').val();
	mail = email

	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
	
	if (email.length == 0) {
		$('#validasiEmail').html('<small><i>*Harap masukkan email</i></small>')
		salah++
	} else if (!pattern.test(email)) {
		$('#validasiEmail').html("<small><i>*Format email salah</i></small>")
		salah++;
	} else { $('#validasiEmail').html('') }


	if (salah == 0) {
		$('#validasiEmail').html('')
		$.ajax({
			url: '/api/emailsearch/' + email,
			type: 'get',
			contentType: 'application/json',
			success: function(data) {

				if (tipe == 'Registration') {
					if (data != null) {
						console.log("sudah ada")
						$('#validasiEmail').html('<small><i>*Email sudah terdaftar</i></small>')
					} else {
						console.log("belum ada")
						$('#validasiEmail').html('')
						mail = email
						modalMasukkanOTP(email)
					}
				} else if (tipe == 'ForgotPassword') {
					if (data == null) {
						console.log("belum ada")
						$('#validasiEmail').html('<small><i>*Email tidak terdaftar</i></small>')
					} else {
						console.log("sudah ada")
						$('#validasiEmail').html('')
						mail = email
						modalMasukkanOTP(email)
					}
				}


			}
		})
	}
}

function countdown() {
	// Set the date we're counting down to
	var countDownResend = (new Date().getTime()) + (1000 * 30 * 1);
	var countDownExpired = (new Date().getTime()) + (1000 * 60 * 1);
	// Update the count down every 1 second
	x = setInterval(function() {

		// Get milis time when clicking
		var now = new Date().getTime();

		// Find the distance between now and the count down time
		var distanceResend = countDownResend - now;

		// Time calculations for min and sec
		var min = Math.floor((distanceResend % (1000 * 60 * 60)) / (1000 * 60));
		var sec = Math.floor((distanceResend % (1000 * 60)) / 1000);

		var distanceExpired = countDownExpired - now;

		// Time calculations for min and sec
		var min2 = Math.floor((distanceExpired % (1000 * 60 * 60)) / (1000 * 60));
		var sec2 = Math.floor((distanceExpired % (1000 * 60)) / 1000);
		// Output the result in an element with id="demo"
		$('#kirimulangOTP').html("Kirim Ulang OTP dalam " + min + ":" + sec)
		$('#expiredOTP').html("Exp dalam " + min2 + ":" + sec2)


		if (distanceResend < 0) {
			$('#kirimulangOTP').html("<a href='#' type='submit' onclick='resendOTP()'>kirim ulang OTP</a>")
		}
		// If the count down is over, write some text 
		if (distanceExpired < 0) {
			clearInterval(x);
			$('#otpkadaluarsa').attr('hidden', false)
			$('#otpsalah').attr('hidden', true)

			$.ajax({
				url: '/api/edit/token/' + otpGen + '',
				type: 'put',
				contentType: 'application/json',
				//data: id,
				success: function() {
					otpExp5 = otpExp4
					otpExp4 = otpExp3
					otpExp3 = otpExp2
					otpExp2 = otpExp1
					otpExp1 = otpGen
					otpGen = 'exp'
				}
			})
		}
	}, 1000);
}

function resendOTP() {
	$('#kirimulangOTP').html("Mohon tunggu, OTP sedang dikirim ke email anda")
	$('#otpkadaluarsa').attr('hidden', true)
	$('#otpsalah').attr('hidden', true)
	$.ajax({
		url: '/api/edit/token/' + otpGen + '',
		type: 'put',
		contentType: 'application/json',
		data: otpGen,
		success: function() {
			otpExp5 = otpExp4
			otpExp4 = otpExp3
			otpExp3 = otpExp2
			otpExp2 = otpExp1
			otpExp1 = otpGen
			otpGen = 'exp'
		}
	})

	$.ajax({
		url: 'api/sendotp/' + mail,
		type: 'get',
		success: function(otpGenerated) {
			otpGen = otpGenerated;
			clearInterval(x);
			saveToken()
			console.log(otpGen)

		}
	})
}

function modalMasukkanOTP(email) {
	console.log("modalMasukkanOTP")
	var teksVerifikasi = "<div class='text-left'>"
	teksVerifikasi += "<p>Masukkan kode OTP yang telah dikirimkan ke email anda</p>"
	teksVerifikasi += "<input style='position: relative; width: 100%; text-align: center;' type='text' id='otp' placeholder='masukkan OTP disini...'>"
	teksVerifikasi += "<p id='otpkadaluarsa' class='text-danger' hidden><small><i>*Kode OTP kadaluarsa, silakan kirim ulang OTP</i></small></p>"
	teksVerifikasi += "<p id='otpsalah' class='text-danger' hidden><small><i>*Kode OTP salah, silakan ulangi OTP</i></small></p>"
	teksVerifikasi += "<p id='kirimulangOTP' style='position: relative; text-align: center;'></p>"
	teksVerifikasi += "<p id='expiredOTP' style='position: relative; text-align: center; ></p>"
	teksVerifikasi += "</div>"

	$.ajax({
		url: '/api/sendotp/' + email,
		type: 'get',
		success: function(otpGenerated) {
			otpGen = otpGenerated;
			clearInterval(x);
			saveToken()
			console.log(otpGen)
		}
	})

	//var otpGen = otpGen1;

	$('.modal-title').html('Verifikasi E-mail')
	$('.modal-body').html(teksVerifikasi)
	$('#buttonSave').off('click').html('Konfirmasi OTP').on('click', function() { modalKonfirmasiOTP($('#otp').val(), email) })
}

function saveToken() {
	var now = new Date().getTime()
	var addMinute = 60 * 1000
	var expired_on = now + addMinute

	var formdata = '{'
	formdata += '"email": "' + mail + '",'
	formdata += '"token": "' + otpGen + '",'
	formdata += '"expiredOn": "' + expired_on + '",'
	formdata += '"usedFor": "' + tipe + '"'
	formdata += '}'

	$.ajax({
		url: '/api/add/token',
		type: 'post',
		contentType: 'application/json',
		data: formdata,
		success: function() {
			countdown();
		}
	})
}

function modalKonfirmasiOTP(otp, email) {
	console.log("Masuk ke modalKonfirmasiOTP")

	if (otp == otpGen) {
		console.log("otp benar")
		$('#otpkadaluarsa').attr('hidden', true)
		$('#otpsalah').attr('hidden', true)
		modalSetPassword(email)
	} else if (otp == otpExp1 || otp == otpExp2 || otp == otpExp3 || otp == otpExp4 || otp == otpExp5) {
		console.log("otp kadaluarsa")
		$('#otpkadaluarsa').attr('hidden', false)
		$('#otpsalah').attr('hidden', true)
	} else {
		console.log("otp salah")
		$('#otpsalah').attr('hidden', false)
		$('#otpkadaluarsa').attr('hidden', true)
	}
}

function modalSetPassword(email) {
	console.log("Masuk ke modalSetPassword")
	var teksSetPassword = "<div class='text-left'>"
	teksSetPassword += "<p>Masukkan password baru untuk akun anda</p>"
	teksSetPassword += "<p style='margin-bottom: 0;'><small>Password</small></p>"
	teksSetPassword += "<div class='input-group mb-3'>"
	teksSetPassword += "<input type='password' id='password' style='position: relative; width: 91%;' required>"
	teksSetPassword += "<div class='input-group-append'>"
	teksSetPassword += "<span class='input-group-text bi bi-eye-slash' id='eyeId' value = '0' onclick='togglePassword()'></span>"
	teksSetPassword += "</div>"
	teksSetPassword += "</div>"
	teksSetPassword += "<p id='validasipassword' class='text-danger' hidden><small><i>*password tidak memenuhi standar, minimal 8 karakter, mengandung huruf besar, huruf kecil, angka, dan special character[!#$%&*()-,.+/@^]</i></small></p>"
	teksSetPassword += "<p style='margin-bottom: 0;'><small>Masukkan ulang password</small></p>"
	teksSetPassword += "<div class='input-group mb-3'>"
	teksSetPassword += "<input type='password' id='password2' style='position: relative; width: 91%;' required>"
	teksSetPassword += "<div class='input-group-append'>"
	teksSetPassword += "<span class='input-group-text bi bi-eye-slash' id='eyeId2' value = '0' onclick='togglePassword2()'></span>"
	teksSetPassword += "</div>"
	teksSetPassword += "</div>"
	teksSetPassword += "<p id='validasipassword2' class='text-danger' hidden></p>"
	teksSetPassword += "<p id='passwordtaksama' class='text-danger' hidden><small><i>*Password tidak sama</i></small></p>"
	teksSetPassword += "</div>"

	$('.modal-title').html('Set Password')
	$('.modal-body').html(teksSetPassword)
	$('#buttonSave').off('click').html('Set Password').on('click', function() { setPassword(($('#password').val()), ($('#password2').val()), email) })
}

function setPassword(password, passwordConfirm, email) {
	console.log("Masuk ke setPassword")
	var salah = 0
	if (password.length == 0) {
		$('#validasipassword').html("<small><i>*mohon masukkan password</i></small>").attr('hidden', false)
		salah++
	} else {$('#validasipassword').attr('hidden', true)}
	
	if (passwordConfirm.length == 0) {
		$('#validasipassword2').html("<small><i>*mohon masukkan password</i></small>").attr('hidden', false)
		salah++
	} else {$('#validasipassword2').attr('hidden', true)}
	
	if (password != passwordConfirm) {
		console.log("pass beda")
		$('#passwordtaksama').attr('hidden', false)
		salah++
	} 
	
	if (salah == 0) {
		console.log("pass sama")
		$('#passwordtaksama').attr('hidden', true)

		$.ajax({
			url: '/api/passwordvalidation/' + password,
			type: 'get',
			contentType: 'application/json',
			success: function(statusValidasi) {
				console.log(statusValidasi)
				console.log(email)
				console.log(password)

				if (statusValidasi == "strong") {
					if (tipe == 'Registration') {
						modalSignUp(email, password)
					} else if (tipe == 'ForgotPassword') {
						modalEditPassword(password)
					}

				} else if (statusValidasi == "weak") {
					$('#validasipassword').attr('hidden', false)
					$('#passwordtaksama').attr('hidden', true)
				}
			}

		})
	}

}

function modalSignUp(email, password) {
	$('#modal').modal('show')
	console.log("Masuk ke modalSignUp")
	var teksSignUp = "<div class='text-left'>"
	teksSignUp += "<p style='margin-bottom: 0;'><small>Nama Lengkap*</small></p><br>"
	teksSignUp += "<input style='position: relative; width: 100%;' type='text' id='fullname' placeholder='masukkan Nama Lengkap disini...'>"
	teksSignUp += "<p id='validasinama' class='text-danger'></p>"
	teksSignUp += "<p style='margin-bottom: 0;'><br><small>Nomor Handphone</small></p>"
	teksSignUp += "<div class='input-group-prepend'>"
	teksSignUp += '<span class="input-group-text" style="position: relative;top: 9px;z-index: 1;width: 54px;height: 32px;">+62</span>'
	teksSignUp += "</div>"
	teksSignUp += "<input style='position: relative;width: 87%;left: 61px;top: -22px;' type='text' id='phone' placeholder='masukkan nomor handphone disini...'>"
	teksSignUp += "<p id='validasinomorhandphone' class='text-danger'></p>"
	teksSignUp += "<p style='margin-bottom: 0;'><small>Daftar Sebagai</small></p>"
	teksSignUp += "<select id='roleid' style='position: relative; width: 100%; text-align: center;'>"
	teksSignUp += "<option value=0 >--Pilih--</option>"
	teksSignUp += "<option value=1 >Admin</option>"
	teksSignUp += "<option value=2 >Pasien</option>"
	teksSignUp += "<option value=3 >Dokter</option>"
	teksSignUp += "</select>"
	teksSignUp += "<p id='validasiRole' class='text-danger'></p>"
	teksSignUp += "</div>"

	$('.modal-title').html('Daftar')
	$('.modal-body').html(teksSignUp)
	$('#buttonSave').off('click').html('Daftar').on('click', function() { addBiodata(email, password) })
}

function modalEditPassword(password) {

	var formEditPass = '{'
	formEditPass += '"email": "' + mail + '",'
	formEditPass += '"password": "' + password + '"'
	formEditPass += '}'

	$.ajax({
		url: '/api/user/edit',
		type: 'put',
		contentType: 'application/json',
		data: formEditPass,
		success: function() {
			var teksModal = "<h4>Password berhasil diupdate, silakan login kembali</h4>"
			$('.modal-title').html('Password Updated')
			$('.modal-body').html(teksModal)
			$('#buttonSave').off('click').html('keluar').on('click', function() { location.href = "/indexlogin" })

		}
	})

}

var bioId

function addBiodata(email, password) {
	var fullname = $('#fullname').val()
	var phone = $('#phone').val()
	var roleid = $('#roleid').val()
	var salah = 0
	

	if (roleid == 0) {
		$('#validasiRole').html('<small><i>*Harap pilih Role</i></small>')
		salah++
	} else {
		$('#validasiRole').html('')
	}
	
	if (fullname.length == 0) {
		$('#validasinama').html('<small><i>*Harap Masukkan Nama</i></small>')
		salah++
	} else {
		$('#validasinama').html('')
	}
	
	if (phone.length == 0) {
		$('#validasinomorhandphone').html('<small><i>*Harap Masukkan Nomor Handphone</i></small>')
		salah++
	} else {
		$('#validasinomorhandphone').html('')
	}
	
	
	if (salah == 0) {

		var formbiodata = '{'
		formbiodata += '"fullName": "' + fullname + '",'
		formbiodata += '"mobilePhone": "+62' + phone + '",'
		formbiodata += '"isDelete": false'
		formbiodata += '}'

		$.ajax({
			url: '/api/biodata/add',
			type: 'post',
			contentType: 'application/json',
			data: formbiodata,
			success: function() {
				console.log("berhasil add biodata, mencari maks biodata id")

				$.ajax({
					url: '/api/getmaxmbiodataid',
					type: 'get',
					contentType: 'application/json',
					success: function(biodataid) {
						bioId = biodataid
						console.log(biodataid)
						console.log(email)
						console.log(roleid)
						console.log(password)

						var formuser = '{'
						formuser += '"email": "' + email + '",'
						formuser += '"roleId": ' + roleid + ','
						formuser += '"biodataId": ' + biodataid + ','
						formuser += '"password": "' + password + '",'
						formuser += '"isDelete": false'
						formuser += '}'

						$.ajax({
							url: '/api/user/add',
							type: 'post',
							contentType: 'application/json',
							data: formuser,
							success: function(data) {
								console.log("berhasil add user")
								console.log(data)
								console.log("role id= " + roleid)

								var userId = data.id

								$.ajax({
									url: '/api/editbiodatacreatedby?id=' + biodataid + '&userid=' + userId,
									type: 'put',
									contentType: 'application/json',
									success: function() {
										console.log("berhasil ubah createby biodata")
									}
								})

								var formuser2 = '{'
								formuser2 += '"biodataId": ' + biodataid + ','
								formuser2 += '"isDelete": false'
								formuser2 += '}'

								if (roleid == 1) {
									var role = 'admin/add'
								} else if (roleid == 2) {
									var role = 'customer/add'
								} else if (roleid == 3) {
									var role = 'doctor/add'
								}
								$.ajax({
									url: '/api/' + role,
									type: 'post',
									contentType: 'application/json',
									data: formuser2,
									success: function() {
										console.log("berhasil add admin/pasien/doctor")
										location.href = "/index";
									}
								})

							}
						})
					}
				})
			}
		})
	}
}

function openModalLupaPassword() {
	tipe = 'ForgotPassword'
	var teksLupaPassword = "<div class='text-left'>"
	teksLupaPassword += "<p>Masukkan email Anda. Kami akan melakukan pengecekan</p>"
	teksLupaPassword += "<p style='margin-bottom: 0;'><small>Email*</small></p>"
	teksLupaPassword += "<input style='position: relative; width: 100%;' type='email' id='email' placeholder='varlkyrie123@mail.com' required>"
	teksLupaPassword += "<p id='validasiEmail' class='text-danger'></p>"
	teksLupaPassword += "</div>"

	$('.modal-title').html('Lupa Password')
	$('.modal-body').html(teksLupaPassword)
	$('#buttonSave').off('click').html('Kirim OTP').on('click', function() { modalSetEmail($('#email').val()) })
	$('.modal-footer').css('justify-content', 'center')
}
































function logout() {
	localStorage.clear();
	location.href = "/indexlogin";
}



