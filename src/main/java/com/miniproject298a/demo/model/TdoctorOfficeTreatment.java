package com.miniproject298a.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_doctor_office_treatment")
public class TdoctorOfficeTreatment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	@Column(name = "doctor_treatment_id")
	private Long doctorTreatmentId;
	@Column(name = "doctor_office_id")
	private Long doctorOfficeId;

	@Column(name = "created_by", nullable = false)
	private Long createdBy;
	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	@Column(name = "modified_by")
	private Long modifyBy;
	@Column(name = "modified_on")
	private Date modifiedOn;
	@Column(name = "deleted_by")
	private Long deletedBy;
	@Column(name = "deleted_on")
	private Date deletedOn;
	@Column(name = "is_delete", nullable = false)
	private Boolean isDelete = false;

	@ManyToOne
	@JoinColumn(name = "doctor_treatment_id", insertable = false, updatable = false)
	private TdoctorOfficeTreatment tDoctorTreatment;

	@ManyToOne
	@JoinColumn(name = "doctor_office_id", insertable = false, updatable = false)
	private TdoctorOffice tDoctorOffice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDoctorTreatmentId() {
		return doctorTreatmentId;
	}

	public void setDoctorTreatmentId(Long doctorTreatmentId) {
		this.doctorTreatmentId = doctorTreatmentId;
	}

	public Long getDoctorOfficeId() {
		return doctorOfficeId;
	}

	public void setDoctorOfficeId(Long doctorOfficeId) {
		this.doctorOfficeId = doctorOfficeId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(Long modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public TdoctorOfficeTreatment gettDoctorTreatment() {
		return tDoctorTreatment;
	}

	public void settDoctorTreatment(TdoctorOfficeTreatment tDoctorTreatment) {
		this.tDoctorTreatment = tDoctorTreatment;
	}

	public TdoctorOffice gettDoctorOffice() {
		return tDoctorOffice;
	}

	public void settDoctorOffice(TdoctorOffice tDoctorOffice) {
		this.tDoctorOffice = tDoctorOffice;
	}
	
}
