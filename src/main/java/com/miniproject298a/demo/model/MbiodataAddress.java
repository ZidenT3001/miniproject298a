package com.miniproject298a.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_biodata_address")
public class MbiodataAddress {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id",  nullable = false)
	private Long id;
	
	@Column(name="label", length = 100)
	private String label;
	
	@Column(name="recipient", length = 100)
	private String recipient;
	
	@Column(name="recipient_phone_number", length=15)
	private String recipientPhoneNumber;
	
	@Column(name="postal_code", length=10)
	private String postalCode;
	
	@Column(name="address", columnDefinition="TEXT")
	private String address;
	
	@Column(name="created_by",  nullable = false)
	private Long createdBy;
	
	@Column(name="created_on",  nullable = false)
	private Date createOn;
	
	@Column(name="modified_by")
	private Long modifiedBy;
	
	@Column(name="modified_on")
	private Date modifiedOn;
	
	@Column(name="deleted_by")
	private Long deletedBy;
	
	@Column(name="deleted_on")
	private Date deletedOn;
	
	@Column(name="is_delete",  nullable = false)
	private Boolean isDelete;
	
	@Column(name="biodata_id")
	private Long biodataId;
	
	@ManyToOne
	@JoinColumn(name="biodata_id", insertable = false, updatable = false)
	private Mbiodata mbiodata;
	
	@Column(name="location_id")
	private Long locationId;
	
	@ManyToOne
	@JoinColumn(name="location_id", insertable = false, updatable = false)
	private Mlocation mlocation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getRecipientPhoneNumber() {
		return recipientPhoneNumber;
	}

	public void setRecipientPhoneNumber(String recipientPhoneNumber) {
		this.recipientPhoneNumber = recipientPhoneNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public Mbiodata getMbiodata() {
		return mbiodata;
	}

	public void setMbiodata(Mbiodata mbiodata) {
		this.mbiodata = mbiodata;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Mlocation getMlocation() {
		return mlocation;
	}

	public void setMlocation(Mlocation mlocation) {
		this.mlocation = mlocation;
	}


	
	
	
}
