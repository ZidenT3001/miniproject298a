package com.miniproject298a.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_doctor_office")
public class TdoctorOffice {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	@Column(name = "doctor_id")
	private Long doctorId;
	@Column(name = "medical_facility_id")
	private Long medicalFacilityId;
	@Column(name = "specialization")
	private String specialization;

	@Column(name = "created_by", nullable = false)
	private Long createdBy;
	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	@Column(name = "modified_by")
	private Long modifyBy;
	@Column(name = "modified_on")
	private Date modifiedOn;
	@Column(name = "deleted_by")
	private Long deletedBy;
	@Column(name = "deleted_on")
	private Date deletedOn;
	@Column(name = "is_delete", nullable = false)
	private Boolean isDelete = false;

	@ManyToOne
	@JoinColumn(name = "medical_facility_id", insertable = false, updatable = false)
	private MmedicalFacility mMedicalFacility;

	@ManyToOne
	@JoinColumn(name = "doctor_id", insertable = false, updatable = false)
	private Mdoctor mDoctor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getMedicalFacilityId() {
		return medicalFacilityId;
	}

	public void setMedicalFacilityId(Long medicalFacilityId) {
		this.medicalFacilityId = medicalFacilityId;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(Long modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public MmedicalFacility getmMedicalFacility() {
		return mMedicalFacility;
	}

	public void setmMedicalFacility(MmedicalFacility mMedicalFacility) {
		this.mMedicalFacility = mMedicalFacility;
	}

	public Mdoctor getmDoctor() {
		return mDoctor;
	}

	public void setmDoctor(Mdoctor mDoctor) {
		this.mDoctor = mDoctor;
	}

}
