package com.miniproject298a.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;

@Entity
@Table(name = "t_customer_chat")
public class TcustomerChat {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "customer_id")
	private Long customer_id;

	@Column(name = "doctor_id")
	private Long doctorId;
	
	@NotNull
	@Column(name = "created_by")
	private Long createdBy;
	
	@NotNull
	@Column(name = "created_on")
	private Date createdOn;
	
	
	@Column(name = "modified_by")
	private Long modifiedBy;
	

	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_by")
	private Long deletedBy;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_delete")
	private Boolean isDelete;
	
	@OneToOne
	@JoinColumn(name = "customer_id", insertable = false, updatable = false)
	private Mcustomer mcustomer ; 
	
	@OneToOne
	@JoinColumn(name = "doctor_id", insertable = false, updatable = false) 
	private Mdoctor mdoctor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Mcustomer getMcustomer() {
		return mcustomer;
	}

	public void setMcustomer(Mcustomer mcustomer) {
		this.mcustomer = mcustomer;
	}

	public Mdoctor getMdoctor() {
		return mdoctor;
	}

	public void setMdoctor(Mdoctor mdoctor) {
		this.mdoctor = mdoctor;
	}

	

}
