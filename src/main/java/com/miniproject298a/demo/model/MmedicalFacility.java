package com.miniproject298a.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_medical_facility")
public class MmedicalFacility {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	@Column(name = "name", length = 50)
	private String name;

	@Column(name = "medical_facility_category_id")
	private Long medicalFacilityCategory_id;
	@Column(name = "location_id")
	private Long locationId;
	@Column(name = "full_address", columnDefinition="TEXT")
	private String fullAddress;
	@Column(name = "email", length = 100)
	private String email;
	@Column(name = "phone_code", length = 10)
	private String phoneCode;
	@Column(name = "phone", length = 15)
	private String phone;
	@Column(name = "fax", length = 15)
	private String fax;

	@Column(name = "created_by", nullable = false)
	private Long createdBy;
	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	@Column(name = "modified_by")
	private Long modifyBy;
	@Column(name = "modified_on")
	private Date modifiedOn;
	@Column(name = "deleted_by")
	private Long deletedBy;
	@Column(name = "deleted_on")
	private Date deletedOn;
	@Column(name = "is_delete", nullable = false)
	private Boolean isDelete = false;
	
	@ManyToOne
	@JoinColumn(name = "medical_facility_category_id", insertable = false, updatable = false)
	private MmedicalFacilityCategory mMedicalFacilityCategory;
	
	@ManyToOne
	@JoinColumn(name = "location_id", insertable = false, updatable = false)
	private Mlocation mLocation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMedicalFacilityCategory_id() {
		return medicalFacilityCategory_id;
	}

	public void setMedicalFacilityCategory_id(Long medicalFacilityCategory_id) {
		this.medicalFacilityCategory_id = medicalFacilityCategory_id;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(Long modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public MmedicalFacilityCategory getmMedicalFacilityCategory() {
		return mMedicalFacilityCategory;
	}

	public void setmMedicalFacilityCategory(MmedicalFacilityCategory mMedicalFacilityCategory) {
		this.mMedicalFacilityCategory = mMedicalFacilityCategory;
	}

	public Mlocation getmLocation() {
		return mLocation;
	}

	public void setmLocation(Mlocation mLocation) {
		this.mLocation = mLocation;
	}
	
}
