package com.miniproject298a.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;


@Entity
@Table(name = "m_customer")
public class Mcustomer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "biodata_id", length = 20)
	private Long biodataId;

	@Column(name = "bod", length = 20)
	private Date bod;

	@NotNull
	@Column(name = "gender")
	private String gender;

	@NotNull
	@Column(name = "blood_group_id")
	private Long bloodGroupId;

	@Column(name = "rhesus_type")
	private String rhesusType;

	@Column(name = "height")
	private Double height;

	@Column(name = "weight")
	private Double weight;

	@NotNull
	@Column(name = "created_by")
	private Long createdBy;
	
	@NotNull
	@Column(name = "created_on")
	private Date createdOn;
	
	
	@Column(name = "modified_by")
	private Long modifiedBy;
	

	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_by")
	private Long deletedBy;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_delete")
	private Boolean isDelete;
	
	@OneToOne
	@JoinColumn(name = "biodata_id", insertable = false, updatable = false)
	private Mbiodata mbiodata ; 
	
	@OneToOne
	@JoinColumn(name = "blood_group_id", insertable = false, updatable = false) 
	private MbloodGroup mbloodGroup;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public Date getBod() {
		return bod;
	}

	public void setBod(Date bod) {
		this.bod = bod;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getBloodGroupId() {
		return bloodGroupId;
	}

	public void setBloodGroupId(Long bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}

	public String getRhesusType() {
		return rhesusType;
	}

	public void setRhesusType(String rhesusType) {
		this.rhesusType = rhesusType;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Mbiodata getMbiodata() {
		return mbiodata;
	}

	public void setMbiodata(Mbiodata mbiodata) {
		this.mbiodata = mbiodata;
	}

	public MbloodGroup getMbloodGroup() {
		return mbloodGroup;
	}

	public void setMbloodGroup(MbloodGroup mbloodGroup) {
		this.mbloodGroup = mbloodGroup;
	}	
	
}
