package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.TdoctorOfficeTreatment;
import com.miniproject298a.demo.model.TdoctorTreatment;

public interface TdoctorTreatmentRepository  extends JpaRepository<TdoctorTreatment, Long>{
	@Query(value="select * from t_doctor_treatment "
			+ "where is_delete = false order by name", nativeQuery = true)
	List<TdoctorTreatment> findAllByIsdelete();
	
	@Query(value="select * from t_doctor_treatment \r\n"
			+ "where doctor_id = ?1 \r\n"
			+ "and  name = ?2 \r\n"
			+ "and is_delete = false ", nativeQuery = true)
	List<TdoctorTreatment> findTindakan (Long iddok, String name);
	
	@Query(value="select * from t_doctor_treatment \r\n"
			+ "where doctor_id = ?1 \r\n"
			+ "and is_delete = false ", nativeQuery = true)
	List<TdoctorTreatment> findTindakanByIdDok (Long iddok);

}
