package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298a.demo.model.MbloodGroup;
import com.miniproject298a.demo.model.Mrole;

public interface MbloodGroupRepository extends JpaRepository<MbloodGroup, Long>{
	
	List<MbloodGroup> findByIsDelete(Boolean isDelete);

}
