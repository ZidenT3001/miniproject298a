package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mdoctor;
import com.miniproject298a.demo.model.TcurrentDoctorSpecialization;
import com.miniproject298a.demo.model.TcustomerChat;
import com.miniproject298a.demo.model.TdoctorOffice;

public interface DokterRepository extends JpaRepository<Mdoctor, Long> {

	@Override
	default List<Mdoctor> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Query(value = "SELECT * FROM m_doctor d JOIN m_biodata b ON d.biodata_id = b.id", nativeQuery = true)
	List<Mdoctor> findByBiodataId();

	// senin
	List<Mdoctor> findByBiodataIdAndIsDelete(Long biodataId, Boolean isDelete);

	@Query(value = "select * from t_current_doctor_specialization cd join m_specialization s on s.id = cd.specialization_id WHERE s.id = ?1 AND cd.is_delete = false ", nativeQuery = true)
	List<TcurrentDoctorSpecialization> findBySpesialis(Long id);

	@Query(value = "select * from m_doctor d join m_biodata b on d.biodata_id = b.id where lower(b.fullName) like lower(concat('%',?1,'%'))  ", nativeQuery = true)
	List<Mdoctor> findByNamaDok(String name);

	@Query(value = "select * from m_doctor d join t_doctor_office dof on d.id = dof.doctor_id join  m_medical_facility mf on mf.id = dof.medical_facility_id where mf.location_id = ?1  ", nativeQuery = true)
	List<TdoctorOffice> findByLokDok(Long id);
//	@Query(value="SELECT * FROM", nativeQuery = true)
//	List<MDoctor> findByNameDok();
	
	@Query(value = "select * from m_doctor d join t_doctor_office dof on d.id = dof.doctor_id join  m_medical_facility mf on mf.id = dof.medical_facility_id join m_location l on mf.location_id = l.id WHERE lower(l.name) LIKE lower(concat('?1'))", nativeQuery = true)
	List<Mdoctor> findByNamaLokasiDok(String name);
	
}
