package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298a.demo.model.MlocationLevel;

public interface MlocationLevelRepository extends JpaRepository<MlocationLevel, Long>{
	List<MlocationLevel> findByIsDelete(Boolean isDelete);
}

