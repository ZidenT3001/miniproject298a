package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298a.demo.model.TresetPassword;

public interface ResetPasswordRepository extends JpaRepository<TresetPassword, Long> {

}
