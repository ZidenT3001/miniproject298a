package com.miniproject298a.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mmenu;
import com.miniproject298a.demo.model.MmenuRole;
import com.miniproject298a.demo.model.Mrole;


public interface MenuRoleRepository extends JpaRepository<MmenuRole, Long> {
	@Query(value = "SELECT * FROM m_menu_role WHERE is_delete = ?1", nativeQuery = true)
	List<MmenuRole> findAll(Boolean isDelete);

	@Query(value = "SELECT * FROM m_menu_role WHERE is_delete = ?1 ORDER BY id", nativeQuery = true)
	Page<MmenuRole> findByIsDelete(Boolean isDelete, Pageable pageable);

	// search hakakses
	@Query(value = "SELECT * FROM m_role r JOIN m_menu_role m ON r.id = m.role_id WHERE (lower(r.code) LIKE lower(concat('%', ?1, '%'))) AND r.is_delete = 'false'", nativeQuery = true)
	List<MmenuRole> searchByKey(String key);
	
//	@Query(value = "SELECT menu_id FROM m_menu_role WHERE menu_id = ", nativeQuery = true)
//	List<MmenuRole> findByMenuId();
	
	@Query(value = "SELECT * FROM m_menu_role WHERE menu_id = :key AND is_delete = 'false'", nativeQuery = true)
	List<MmenuRole> findByMenuId(Long key);
	
	List<MmenuRole> findByMenuIdAndIsDelete(Long id, Boolean IsDelete);
	
	List<MmenuRole> findByRoleIdAndIsDelete(Long id, Boolean isDelete);
	
	@Query(value = "SELECT * FROM m_menu_role WHERE role_id = ?1 AND menu_id = ?2 AND is_delete = ?3", nativeQuery = true)
	Optional<MmenuRole> findByRoleIdAndMenuIdAndIsDelete(Long id, Long menuId, Boolean isDelete);
	
	
}
