package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mmenu;
import com.miniproject298a.demo.model.MmenuRole;
import com.miniproject298a.demo.model.Mrole;

public interface AturAksesMenuRepository extends JpaRepository<Mmenu, Long>{
	@Query(value = "SELECT * FROM m_menu WHERE parent_id IS NULL", nativeQuery = true)
	List<Mmenu> findAll();
	
	@Query(value = "SELECT * FROM m_menu WHERE is_delete = ?1 ORDER BY id", nativeQuery = true)
	List<Mmenu> findByIsDelete(Boolean isDelete);
	
	
}
