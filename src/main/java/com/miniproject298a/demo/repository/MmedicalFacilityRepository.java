package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.MmedicalFacility;

public interface MmedicalFacilityRepository extends JpaRepository<MmedicalFacility, Long> {

	@Query(value = "SELECT * FROM m_medical_facility where location_id = ?1 and is_delete = false", nativeQuery = true)
	List<MmedicalFacility> findLocationById(Long LocationId);

}
