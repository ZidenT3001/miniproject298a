package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mcostumer;
import com.miniproject298a.demo.model.MmenuRole;
import com.miniproject298a.demo.model.Mrole;

public interface McostumerRepository extends JpaRepository<Mcostumer, Long> {

	@Query(value = "SELECT * FROM m_costumer c JOIN m_biodata b ON c.biodata_id = b.id  WHERE c.is_delete = ?1 ORDER BY b.fullname", nativeQuery = true)
	List<Mcostumer> findByIsDelete(Boolean isDelete);
	
	@Query(value = "SELECT * FROM m_costumer c JOIN m_biodata b ON c.biodata_id = b.id  WHERE c.is_delete = ?1 ORDER BY b.fullname DESC", nativeQuery = true)
	List<Mcostumer> findByIsDeleteDesc(Boolean isDelete);

	// search hakakses	
	@Query(value = "SELECT * FROM m_costumer m JOIN m_biodata b ON b.id = m.biodata_id WHERE (lower(b.fullname) LIKE lower(concat('%', ?1, '%'))) AND m.is_delete = 'false'", nativeQuery = true)
	List<Mcostumer> searchByKey(String key);
	
	@Query(value = "SELECT * FROM m_costumer c JOIN m_biodata b ON c.biodata_id = b.id  WHERE c.is_delete = ?1 ORDER BY b.fullname", nativeQuery = true)
	Page<Mcostumer> findByIsDelete(Boolean isDelete, Pageable pageable);

}
