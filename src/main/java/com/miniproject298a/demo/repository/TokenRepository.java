package com.miniproject298a.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.miniproject298a.demo.model.Token;

public interface TokenRepository extends JpaRepository<Token, Long> {

	List<Token> findByIsExpired(Boolean isExpired);
	
	@Query(value = "SELECT * FROM t_token WHERE token = ?1 AND is_delete IS NOT true", nativeQuery = true)
	Optional<Token> findByToken(String token);
}
