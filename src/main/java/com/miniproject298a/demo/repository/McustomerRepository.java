package com.miniproject298a.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mcustomer;
import com.miniproject298a.demo.model.MmenuRole;
import com.miniproject298a.demo.model.Mrole;

public interface McustomerRepository extends JpaRepository<Mcustomer, Long> {
	
	List<Mcustomer> findByIsDelete(Boolean isDelete);
	
	@Query(value = "SELECT EXTRACT(YEAR from AGE(bod)) as age FROM m_customer WHERE id =?1", nativeQuery = true)
	Long findByDob(Long id);
	
	@Query(value = "SELECT EXTRACT(YEAR from AGE(bod)) as age, biodata_id  FROM m_customer WHERE is_delete =?1", nativeQuery = true)
	List<Mcustomer> findByDobList(Boolean IsDelete);
	
	List<Mcustomer> findByBiodataId(Long biodataId);
	
	@Query(value = "SELECT * FROM m_customer WHERE biodata_id = ?1", nativeQuery = true)
	Optional<Mcustomer> findByIdBiodataPasien(Long idpasien);
	
}
