package com.miniproject298a.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mcustomer;
import com.miniproject298a.demo.model.McustomerMember;

public interface McustomerMemberRepository extends JpaRepository<McustomerMember, Long> {

	@Query(value = "SELECT * FROM m_customer_member a JOIN m_customer b ON a.customer_id = b.id JOIN m_biodata c ON b.biodata_id = c.id WHERE a.is_delete = false AND b.is_delete = false AND c.is_delete = false AND a.parent_biodata_id = ?1 ORDER BY c.fullName", nativeQuery = true)
	List<McustomerMember> findByParentBiodataIdAndIsDeleted(Long idparent);

	List<McustomerMember> findByIsDelete(Boolean isDelete);

	@Query(value = "SELECT * FROM m_customer_member WHERE customer_id = ?1", nativeQuery = true)
	Optional<McustomerMember> findByIdCustomer(Long idcust);

	@Query(value = "SELECT * FROM m_customer_member a\r\n" + "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n"
			+ "WHERE a.parent_biodata_id = ?1 AND (lower(fullName) LIKE lower(concat('%', ?2, '%'))) AND a.is_delete = false AND\r\n"
			+ "b.is_delete = false AND\r\n" + "c.is_delete = false ORDER BY c.fullName", nativeQuery = true)
	List<McustomerMember> searchByName(Long id, String key);

	@Query(value = "SELECT * FROM m_customer_member a\r\n" + "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n" + "WHERE a.is_delete = false AND\r\n"
			+ "b.is_delete = false AND\r\n" + "c.is_delete = false AND a.parent_biodata_id = ?1\r\n"
			+ "ORDER BY b.bod", nativeQuery = true)
	List<McustomerMember> sortByAge(Long id);

	@Query(value = "SELECT * FROM m_customer_member a JOIN m_customer b ON a.customer_id = b.id JOIN m_biodata c ON b.biodata_id = c.id WHERE a.is_delete = false AND b.is_delete = false AND c.is_delete = false AND a.parent_biodata_id = ?1 ORDER BY c.fullName DESC", nativeQuery = true)
	List<McustomerMember> findByParentBiodataIdAndIsDeletedDesc(Long idparent);

	@Query(value = "SELECT * FROM m_customer_member a\r\n" + "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n" + "WHERE a.is_delete = false AND\r\n"
			+ "b.is_delete = false AND\r\n" + "c.is_delete = false AND a.parent_biodata_id = ?1\r\n"
			+ "ORDER BY b.bod DESC", nativeQuery = true)
	List<McustomerMember> sortByAgeDesc(Long id);

//	// paging
//	// asc nama
//
	@Query(value = "SELECT * FROM m_customer_member a JOIN m_customer b ON a.customer_id = b.id JOIN m_biodata c ON b.biodata_id = c.id WHERE a.is_delete = false AND b.is_delete = false AND c.is_delete = false AND a.parent_biodata_id = ?1 ORDER BY c.fullName", nativeQuery = true)
	Page<McustomerMember> sortByAsc(Long idparent, Pageable pageable);

	@Query(value = "SELECT * FROM m_customer_member a JOIN m_customer b ON a.customer_id = b.id JOIN m_biodata c ON b.biodata_id = c.id WHERE a.is_delete = false AND b.is_delete = false AND c.is_delete = false AND a.parent_biodata_id = ?1 ORDER BY c.fullName DESC", nativeQuery = true)
	Page<McustomerMember> sortByDesc(Long idparent, Pageable pageable);

	@Query(value = "SELECT * FROM m_customer_member a\r\n" + "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n" + "WHERE a.is_delete = false AND\r\n"
			+ "b.is_delete = false AND\r\n" + "c.is_delete = false AND a.parent_biodata_id = ?1\r\n"
			+ "ORDER BY b.bod", nativeQuery = true)
	Page<McustomerMember> sortAgeAsc(Long id, Pageable pageable);

	@Query(value = "SELECT * FROM m_customer_member a\r\n" + "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n" + "WHERE a.is_delete = false AND\r\n"
			+ "b.is_delete = false AND\r\n" + "c.is_delete = false AND a.parent_biodata_id = ?1\r\n"
			+ "ORDER BY b.bod DESC", nativeQuery = true)
	Page<McustomerMember> sortAgeDesc(Long id, Pageable pageable);

}
