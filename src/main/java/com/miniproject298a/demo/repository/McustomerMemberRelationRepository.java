package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298a.demo.model.Mcustomer;
import com.miniproject298a.demo.model.McustomerRelation;

public interface McustomerMemberRelationRepository extends JpaRepository<McustomerRelation, Long>{
	
	List<McustomerRelation> findByIsDelete(Boolean isDelete);
	
	

}
