package com.miniproject298a.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mbiodata;

public interface MbiodataRepository extends JpaRepository<Mbiodata, Long> {
	
	@Query("SELECT MAX(id) FROM Mbiodata")
	public Long findByMaxId();
	
	@Query(value = "SELECT * FROM m_biodata WHERE fullname = ?1 AND created_by = ?2 AND is_delete = ?3", nativeQuery = true)
	Optional<Mbiodata> findByNameCreateByAndIsDelete(String name, Long createdBy, Boolean isDelete);

}
