package com.miniproject298a.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Madmin;

public interface MadminRepository extends JpaRepository<Madmin, Long> {
	

}
