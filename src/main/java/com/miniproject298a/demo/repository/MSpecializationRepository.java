package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mdoctor;
import com.miniproject298a.demo.model.Mspecialization;

public interface MSpecializationRepository extends JpaRepository<Mspecialization, Long> {

	@Query(value = "select * from m_specialization " + "where is_delete = false ", nativeQuery = true)
	List<Mspecialization> findAllByIsdelete();

}