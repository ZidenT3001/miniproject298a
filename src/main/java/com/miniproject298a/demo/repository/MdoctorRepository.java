package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298a.demo.model.Mdoctor;

public interface MdoctorRepository extends JpaRepository<Mdoctor, Long> {
	List<Mdoctor> findByBiodataIdAndIsDelete(Long biodataId, Boolean isDelete);

}