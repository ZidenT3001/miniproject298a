package com.miniproject298a.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mbiodata;
import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.model.Muser;

public interface HakAksesRepository extends JpaRepository<Mrole, Long>{
	
	@Query(value = "SELECT * FROM m_role WHERE is_delete = ?1 ORDER BY code", nativeQuery = true)
	List<Mrole> findByIsDelete(Boolean isDelete);	
	
	//search hakakses
	@Query(value = "SELECT * FROM m_role WHERE (lower(code) LIKE lower(concat('%', ?1, '%'))) AND is_delete = 'false'", nativeQuery = true)
	List<Mrole> searchByKey(String key);
	
	@Query(value = "SELECT * FROM m_role WHERE is_delete = ?1 ORDER BY code", nativeQuery = true)	
	Page<Mrole> findByIsDelete(Boolean isDelete, Pageable pageable);
	
	//Descending order
	@Query(value = "SELECT * FROM m_role WHERE is_delete = ?1 ORDER BY code DESC", nativeQuery = true)	
	Page<Mrole> findByIsDeleteDesc(Boolean isDelete, Pageable pageable);
	
	//role doubleHandle
	@Query(value = "SELECT * FROM m_role WHERE name = ?1 OR code = ?2 AND is_delete = ?3", nativeQuery = true)
	Optional<Mrole> findByNameCodeAndIsdeledte(String name, String code, Boolean isDelete);
	
	
//	@Query(value = "SELECT * FROM m_role m JOIN m_user u ON m.created_by = u.id JOIN m_biodata b ON b.id = u.biodata_id", nativeQuery = true)
//	List<Mrole> findNamaByUserAndBiodata();
}
