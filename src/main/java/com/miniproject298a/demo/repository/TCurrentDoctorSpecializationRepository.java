package com.miniproject298a.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mspecialization;
import com.miniproject298a.demo.model.TcurrentDoctorSpecialization;

public interface TCurrentDoctorSpecializationRepository extends JpaRepository<TcurrentDoctorSpecialization, Long> {
	
	List<TcurrentDoctorSpecialization> findByIdAndIsDelete(Long id, Boolean isDelete);

	List<TcurrentDoctorSpecialization> findByDoctorIdAndIsDelete(Long doctorId, Boolean isDelete);

	@Query(value = "select * from t_current_doctor_specialization cd join m_specialization s on s.id = cd.specialization_id WHERE s.id = ?1 AND cd.is_delete = false", nativeQuery = true)
	List<TcurrentDoctorSpecialization> findBySpesialis(Long id);
	
	@Query(value="select * \r\n"
			+ "from t_current_doctor_specialization cd \r\n"
			+ "join m_specialization s \r\n"
			+ "on s.id = cd.specialization_id \r\n"
			+ "join m_doctor d\r\n"
			+ "on d.id = cd.doctor_id\r\n"
			+ "WHERE d.biodata_id = ?1\r\n"
			+ "AND cd.is_delete = false", nativeQuery = true)
	List<TcurrentDoctorSpecialization> findCurrentSpecializationByBiodataId(Long biodataId);
	

}