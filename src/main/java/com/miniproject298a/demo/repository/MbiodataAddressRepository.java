package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.MbiodataAddress;

public interface MbiodataAddressRepository extends JpaRepository <MbiodataAddress, Long> {
	
	@Query(value = "SELECT * FROM m_biodata_address where location_id = ?1 and is_delete = false", nativeQuery = true)
	List<MbiodataAddress> findLocationById(Long LocationId);
	

}
