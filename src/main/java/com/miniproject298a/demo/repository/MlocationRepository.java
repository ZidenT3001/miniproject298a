package com.miniproject298a.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mlocation;

public interface MlocationRepository extends JpaRepository<Mlocation, Long> {

	@Query(value = "SELECT * FROM m_location WHERE is_delete = ?1 ORDER BY id", nativeQuery = true)
	List<Mlocation> findByIsDelete(Boolean isDelete);

	List<Mlocation> findByParentId(Long id);

	// search lokasi
	@Query(value = "SELECT * FROM m_location WHERE (lower(name) LIKE lower(concat('%', ?1, '%'))) AND is_delete = false ORDER BY name", nativeQuery = true)
	List<Mlocation> searchByKey(String key);

	@Query(value = "SELECT * FROM m_location WHERE (lower(name) LIKE lower(concat('%', ?1, '%'))) AND is_delete = false AND location_level_id = ?2 ORDER BY name", nativeQuery = true)
	List<Mlocation> searchByKeyword(String key, int locationLevelId);

	@Query(value = "SELECT * FROM m_location WHERE is_delete = ?1 ORDER BY name", nativeQuery = true)
	Page<Mlocation> findByIsDelete(Boolean isDelete, Pageable pageable);

	@Query(value = "SELECT * FROM m_location WHERE is_delete = false AND location_level_id = ?1 ORDER BY name", nativeQuery = true)
	Page<Mlocation> findByIsDeleteAndLevelPageable(int locationLevelId, Pageable pageable);

	@Query(value = "SELECT * FROM m_location WHERE is_delete = false AND location_level_id = ?1 ORDER BY name", nativeQuery = true)
	List<Mlocation> findByIsDeleteAndLevel(int locationLevelId);

	// Descending order
	@Query(value = "SELECT * FROM m_location WHERE is_delete = ?1 ORDER BY name DESC", nativeQuery = true)
	Page<Mlocation> findByIsDeleteDesc(Boolean isDelete, Pageable pageable);

	@Query(value = "select * from m_location", nativeQuery = true)
	List<Mlocation> findAllByLevel();

	@Query(value = "select * from m_doctor d join t_doctor_office dof on d.id = dof.doctor_id join  m_medical_facility mf on mf.id = dof.medical_facility_id join m_location l on mf.location_id = l.id WHERE lower(l.name) LIKE lower(concat('?1'))", nativeQuery = true)
	List<Mlocation> findByNamaLokasiDok(String name);

}
