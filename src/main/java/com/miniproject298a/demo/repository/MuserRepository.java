package com.miniproject298a.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.model.Muser;

public interface MuserRepository extends JpaRepository<Muser, Long> {

	@Query(value = "SELECT * FROM m_user WHERE lower(email) = lower(?) AND is_locked IS NOT true AND is_delete IS NOT true", nativeQuery = true)
	Optional<Muser> findEmailByKeyword(String keyword);

	// Descending order
	@Query(value = "SELECT * FROM m_user WHERE is_delete = ?1", nativeQuery = true)
	Page<Muser> findByIsDelete(Boolean isDelete, Pageable pageable);
	
	Optional<Muser> findById(Long id);

}
