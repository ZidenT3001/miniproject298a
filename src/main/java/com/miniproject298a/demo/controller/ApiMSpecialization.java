package com.miniproject298a.demo.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mspecialization;
import com.miniproject298a.demo.repository.MSpecializationRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/") 
public class ApiMSpecialization {
		@Autowired
		private MSpecializationRepository mSpecializationRepository;

		@GetMapping("mspecialization")
		public ResponseEntity<List<Mspecialization>> getMSpecialization() {
			try {
				List<Mspecialization> mSpecialization = this.mSpecializationRepository.findAll();

				ResponseEntity rest = new ResponseEntity<>(mSpecialization, HttpStatus.OK);
				return rest;

			} catch (Exception e) {
				return new ResponseEntity<List<Mspecialization>>(HttpStatus.NO_CONTENT);
			}
		}

	}