package com.miniproject298a.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.repository.McustomerRepository;
import com.miniproject298a.demo.repository.TappointMentRepository;


@RestController
@RequestMapping("/api/")
public class ApiTappointmentController {
	
	@Autowired
	public TappointMentRepository tappointMentRepository;
	
	@GetMapping("appointment/{id}")
	public ResponseEntity<Long> getAllAppointment(@PathVariable("id") Long id) {
		try {
			Long chat = this.tappointMentRepository.findByIsAppointment(id);
			return new ResponseEntity<Long>(chat, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("get/totalappointment/doctor/{doctorId}")
	public ResponseEntity<Long> getTotalAppointmentByDoctorId(@PathVariable("doctorId") Long doctorId) {
		try {
			Long totalChat = this.tappointMentRepository.findTotalAppoinmentByDoctorId(doctorId);
			return new ResponseEntity<Long>(totalChat, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

}
