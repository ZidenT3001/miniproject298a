package com.miniproject298a.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/costumer/")
public class CostumerController {
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("costumer/indexapi");
		return view;
	}
	
	@GetMapping("style")
	public ModelAndView css() {
		ModelAndView view = new ModelAndView("costumer/css");
		return view;
	}

}
