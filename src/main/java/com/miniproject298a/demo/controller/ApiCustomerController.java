package com.miniproject298a.demo.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.miniproject298a.demo.model.Mcustomer;
import com.miniproject298a.demo.model.McustomerMember;
import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.repository.HakAksesRepository;
import com.miniproject298a.demo.repository.McustomerMemberRepository;
import com.miniproject298a.demo.repository.McustomerRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCustomerController {

	@Autowired
	public McustomerRepository customerRepository;

	@Autowired
	public McustomerMemberRepository customerMemberRepository;

	@GetMapping("mcustomer")
	public ResponseEntity<List<Mcustomer>> getAllDataPasien() {
		try {
			List<Mcustomer> listCustomer = this.customerRepository.findByIsDelete(false);
			return new ResponseEntity<List<Mcustomer>>(listCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Mcustomer>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("mcustomer/{id}")
	public ResponseEntity<List<Mcustomer>> getCustomerById(@PathVariable("id") Long id) {
		try {
			Optional<Mcustomer> customerById = this.customerRepository.findById(id);
			if (customerById.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(customerById, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Mcustomer>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("mcustomer/biodata/{biodataId}")
	public ResponseEntity<List<Mcustomer>> getCustomerByBiodataId(@PathVariable("biodataId") Long biodataId) {
		try {
			List<Mcustomer> customerById = this.customerRepository.findByBiodataId(biodataId);
			ResponseEntity rest = new ResponseEntity<>(customerById, HttpStatus.OK);
			return rest;
		} catch (Exception e) {
			return new ResponseEntity<List<Mcustomer>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("mcustomer/age")
	public ResponseEntity<List<Mcustomer>> getAllAgePasien() {
		try {
			List<Mcustomer> agePasien = this.customerRepository.findByDobList(false);
			return new ResponseEntity<>(agePasien, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("mcustomer/age/{id}")
	public ResponseEntity<Long> getAgePasien(@PathVariable("id") Long id) {
		try {
			Long age = this.customerRepository.findByDob(id);
			return new ResponseEntity<Long>(age, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/mcustomer/{id}")
	public ResponseEntity<Object> editdataCustomerByBiodataId(@PathVariable("id") long id,
			@RequestBody Mcustomer mCustomer) {
		Optional<Mcustomer> mCustomerData = this.customerRepository.findById(id);
		if (mCustomerData.isPresent()) {
			mCustomer.setId(id);
			mCustomer.setBiodataId(mCustomerData.get().getBiodataId());
			mCustomer.setCreatedBy(mCustomerData.get().getCreatedBy());
			mCustomer.setCreatedOn(mCustomerData.get().getCreatedOn());
			mCustomer.setGender(mCustomerData.get().getGender());
			mCustomer.setHeight(mCustomerData.get().getHeight());
			mCustomer.setWeight(mCustomerData.get().getWeight());
			mCustomer.setModifiedOn(Date.from(Instant.now()));
			this.customerRepository.save(mCustomer);
			return new ResponseEntity<Object>("Upload Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// API Kristy
	@PostMapping("customerfamily/add")
	public ResponseEntity<Object> saveBiodataCustomer(@RequestBody Mcustomer mCustomer,
			@RequestParam("idRelation") Long idrelation, @RequestParam("idBiodata") Long idbiodata) {

		mCustomer.setCreatedOn(new Date());
		mCustomer.setIsDelete(false);

		Mcustomer custData = this.customerRepository.save(mCustomer);
		if (custData.equals(mCustomer)) {
			Long maxIdCustomer = custData.getId();
			McustomerMember customerMember = new McustomerMember();
			customerMember.setCreatedBy(mCustomer.getCreatedBy());
			customerMember.setCreatedOn(new Date());
			customerMember.setIsDelete(false);
			customerMember.setCustomerRelationId(idrelation);
			customerMember.setParentBiodataId(idbiodata);
			customerMember.setCustomerId(maxIdCustomer);
			this.customerMemberRepository.save(customerMember);
			return new ResponseEntity<>("Save Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("customerfamily/{id}/edit")
	public ResponseEntity<Object> editBiodataCustomer(@RequestBody Mcustomer mCustomer,
			@PathVariable("id") Long idPasien, @RequestParam("idRelation") Long idrelation,
			@RequestParam("idBiodata") Long idbiodata) {
		Optional<Mcustomer> datacust = this.customerRepository.findByIdBiodataPasien(idPasien);
		if (datacust.isPresent()) {
			mCustomer.setId(datacust.get().getId());
			mCustomer.setBiodataId(datacust.get().getBiodataId());
			mCustomer.setCreatedBy(datacust.get().getCreatedBy());
			mCustomer.setCreatedOn(datacust.get().getCreatedOn());
			mCustomer.setIsDelete(false);
			mCustomer.setModifiedOn(new Date());
			mCustomer.setModifiedBy(idbiodata);

			// MCustomer custData =
			this.customerRepository.save(mCustomer);
			// if (custData.equals(mCustomer)) {
			Long idCustomer = datacust.get().getId();
			Optional<McustomerMember> datamember = this.customerMemberRepository.findByIdCustomer(idCustomer);
			if (datamember.isPresent()) {
				McustomerMember customerMember = new McustomerMember();
				customerMember.setId(datamember.get().getId());
				customerMember.setCreatedBy(mCustomer.getCreatedBy());
				customerMember.setCreatedOn(datamember.get().getCreatedOn());
				customerMember.setCustomerId(datamember.get().getCustomerId());
				customerMember.setCustomerRelationId(idrelation);
				customerMember.setIsDelete(false);
				customerMember.setModifiedBy(mCustomer.getCreatedBy());
				customerMember.setModifiedOn(new Date());
				customerMember.setParentBiodataId(idbiodata);
				this.customerMemberRepository.save(customerMember);
				return new ResponseEntity<>("Save Success", HttpStatus.OK);
			} else {
				return ResponseEntity.notFound().build();
			}

//			} else {
//				return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
//			}
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

}
