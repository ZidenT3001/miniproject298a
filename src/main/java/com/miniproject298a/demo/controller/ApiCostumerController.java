package com.miniproject298a.demo.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mcostumer;
import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.repository.HakAksesRepository;
import com.miniproject298a.demo.repository.McostumerRepository;

@RestController
@RequestMapping("/api/")
public class ApiCostumerController {
	
	@Autowired
	public McostumerRepository mcostumerRepository;
	
	@GetMapping("costumer")
	public ResponseEntity<List<Mcostumer>> getAllcostumer() {
		try {
			List<Mcostumer> listCustomer = this.mcostumerRepository.findByIsDelete(false);
			return new ResponseEntity<List<Mcostumer>>(listCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("costumer/{id}")
	public ResponseEntity<List<Mcostumer>> getCostumerById(@PathVariable("id") Long id){
		try {
			Optional<Mcostumer> mcostumer = this.mcostumerRepository.findById(id);
			if (mcostumer.isPresent()) {
				ResponseEntity rest = new ResponseEntity(mcostumer, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Mcostumer>>(HttpStatus.NO_CONTENT);
		}
	}
	

	@PostMapping("costumer/add")
	public ResponseEntity<Object> saveCostumer(@RequestBody Mcostumer mcostumer){
		mcostumer.setCreatedOn(new Date());
		mcostumer.setIsDelete(false);
		Mcostumer mroleData = this.mcostumerRepository.save(mcostumer);
		if(mroleData.equals(mcostumer)) {
			return new ResponseEntity<>("save data successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@PutMapping("edit/costumer/{id}")
	public ResponseEntity<Object> editCostumer(@PathVariable("id") Long id, @RequestBody Mcostumer mcostumer) {
		Optional<Mcostumer> mcostumerData = this.mcostumerRepository.findById(id);
		if (mcostumerData.isPresent()) {
			mcostumer.setId(id);
			mcostumer.setModifiedBy(id);
			mcostumer.setModifiedOn(Date.from(Instant.now()));
			mcostumer.setCreatedBy(id);
			mcostumer.setIsDelete(false);
			mcostumer.setCreatedOn(Date.from(Instant.now()));
			this.mcostumerRepository.save(mcostumer);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}


	// dalete category
	@PutMapping("delete/costumer/{id}")
	public ResponseEntity<Object> deleteHakAkses(@PathVariable("id") Long id) {
		Optional<Mcostumer> mcostumerData = this.mcostumerRepository.findById(id);
		if (mcostumerData.isPresent()) {
			Mcostumer mcostumer = new Mcostumer();
			mcostumer.setId(id);
			mcostumer.setIsDelete(true);
			mcostumer.setDeletedBy(id);
			mcostumer.setDeletedOn(new Date());
			mcostumer.setCreatedBy(mcostumerData.get().getCreatedBy());
			mcostumer.setCreatedOn(mcostumerData.get().getCreatedOn());
			this.mcostumerRepository.save(mcostumer);
			return new ResponseEntity<>("Deleted Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("costumer/pagging")
	public ResponseEntity<Map<String, Object>> getCostumer(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size){
		try {
			List<Mcostumer> mcostumer = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<Mcostumer> pageTuts;
			pageTuts = this.mcostumerRepository.findByIsDelete(false, pagingSort);
			mcostumer = pageTuts.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("variants", mcostumer);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
						
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
//	@GetMapping("costumer/pagging")
//	public ResponseEntity<Map<String, Object>> getCostumerDesc(@RequestParam(defaultValue = "0") int page,
//			@RequestParam(defaultValue = "5") int size){
//		try {
//			List<Mcostumer> mcostumer = new ArrayList<>();
//			Pageable pagingSort = PageRequest.of(page, size);
//			
//			Page<Mcostumer> pageTuts;
//			pageTuts = this.mcostumerRepository.findByIsDeleteDesc(false, pagingSort);
//			mcostumer = pageTuts.getContent();
//			Map<String, Object> response = new HashMap<>();
//			response.put("variants", mcostumer);
//			response.put("currentPage", pageTuts.getNumber());
//			response.put("totalItems", pageTuts.getTotalElements());
//			response.put("totalPage", pageTuts.getTotalPages());
//			
//			return new ResponseEntity<>(response, HttpStatus.OK);
//						
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
	
	@GetMapping("cariCostumer/{key}")
	public ResponseEntity<List<Mcostumer>> searchCategory(@PathVariable("key") String key) {
		try {
			List<Mcostumer> listCategory = this.mcostumerRepository.searchByKey(key);
			return new ResponseEntity<List<Mcostumer>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	

}
