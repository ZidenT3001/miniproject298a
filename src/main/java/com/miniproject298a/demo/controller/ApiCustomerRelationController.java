package com.miniproject298a.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.McustomerRelation;
import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.repository.HakAksesRepository;
import com.miniproject298a.demo.repository.McustomerMemberRelationRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCustomerRelationController {
	
	@Autowired
	public McustomerMemberRelationRepository mcustomerMemberRelationRepository;
	
	@GetMapping("customerrelation")
	public ResponseEntity<List<McustomerRelation>> getAllDataCustomerRelation() {
		try {
			List<McustomerRelation> listCustomerRelation = this.mcustomerMemberRelationRepository.findByIsDelete(false);
			return new ResponseEntity<List<McustomerRelation>>(listCustomerRelation, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
