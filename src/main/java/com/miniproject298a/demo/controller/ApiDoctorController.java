package com.miniproject298a.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mdoctor;
import com.miniproject298a.demo.repository.MdoctorRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiDoctorController {
	
	@Autowired
	public MdoctorRepository mdoctorRepository;
	
	@PostMapping("doctor/add")
	public ResponseEntity<Object> saveDoctor(@RequestBody Mdoctor mdoctor){
		mdoctor.setCreatedOn(new Date());
		mdoctor.setIsDelete(false);
		Mdoctor mdoctorData = this.mdoctorRepository.save(mdoctor);
		if(mdoctorData.equals(mdoctor)) {
			return new ResponseEntity<>("save data successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("mdoctor/biodata/{id}")
	public ResponseEntity<List<Mdoctor>> getMDoctorByBiodataId(@PathVariable("id") Long id) {
		try {
			List<Mdoctor> listMdoctor = this.mdoctorRepository.findByBiodataIdAndIsDelete(id, false);
			return new ResponseEntity<List<Mdoctor>>(listMdoctor, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Mdoctor>>(HttpStatus.NO_CONTENT);
		}
	}

}
