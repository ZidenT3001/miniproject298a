package com.miniproject298a.demo.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mmenu;
import com.miniproject298a.demo.model.MmenuRole;
import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.repository.AturAksesMenuRepository;
import com.miniproject298a.demo.repository.MenuRoleRepository;

@RestController
@RequestMapping("/api/")
public class ApiMenuRoleController {

	@Autowired
	public MenuRoleRepository menuRoleRepository;

	@GetMapping("menuRole/list")
	public List<MmenuRole> findAll() {
		return menuRoleRepository.findAll(false);
	}
	
	@GetMapping("menuRole/getMenuByRoleId/{id}")
	public ResponseEntity<List<MmenuRole>> getAllDataHakKases(@PathVariable("id") Long id) {
		try {
			List<MmenuRole> ListMenuRole = this.menuRoleRepository.findAll(false);
			return new ResponseEntity<List<MmenuRole>>(ListMenuRole, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	

	@GetMapping("manuRoleByMenuId/{id}")
	public ResponseEntity<List<MmenuRole>> getMenurRoleByMenuId(@PathVariable("id") Long id) {
		try {
			List<MmenuRole> ListMenuRole = this.menuRoleRepository.findByMenuIdAndIsDelete(id, false);
			return new ResponseEntity<List<MmenuRole>>(ListMenuRole, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("menuRole/add")
	public ResponseEntity<Object> saveMenuRole(@RequestBody MmenuRole mmenuRole) {
		
		Long idRole = mmenuRole.getRoleId();
		Long idMenu = mmenuRole.getMenuId();
		Optional<MmenuRole> menuRoleData = this.menuRoleRepository.findByRoleIdAndMenuIdAndIsDelete(idRole, idMenu, false);
		
		
		if(menuRoleData.isEmpty()) {
			//mmenuRole.setCreatedBy();
			mmenuRole.setIsDelete(false);
			mmenuRole.setCreatedOn(new Date());
			MmenuRole menuRoleData2 = this.menuRoleRepository.save(mmenuRole);
			if (menuRoleData2.equals(mmenuRole)) {
				return new ResponseEntity<>("save data successfully", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
			}			
		} else {
			return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@PutMapping("menuRole/edit/{idRole}")
	public ResponseEntity<Object> editMenuRole(@PathVariable("idRole") Long idRole, @RequestBody MmenuRole mmenuRole) {
		
		Long idMenu = mmenuRole.getMenuId();
		
		Optional<MmenuRole> mroleData = this.menuRoleRepository.findByRoleIdAndMenuIdAndIsDelete(idRole, idMenu, false);
		
		if (mroleData.isPresent()) {
			mmenuRole.setModifiedBy(mroleData.get().getCreatedBy());
			mmenuRole.setModifiedOn(new Date());
			mmenuRole.setCreatedBy(mroleData.get().getCreatedBy());
			mmenuRole.setIsDelete(true);
			mmenuRole.setCreatedOn(mroleData.get().getCreatedOn());
			mmenuRole.setId(mroleData.get().getId());
			mmenuRole.setRoleId(idRole);
			this.menuRoleRepository.save(mmenuRole);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	

	@GetMapping("cariMenuRole/{key}")
	public ResponseEntity<List<MmenuRole>> searchMenuRole(@PathVariable("key") String key) {
		try {
			List<MmenuRole> listCategory = this.menuRoleRepository.searchByKey(key);
			return new ResponseEntity<List<MmenuRole>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("menuRole/pagging")
	public ResponseEntity<Map<String, Object>> getAllMenuRole(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<MmenuRole> menuRole = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<MmenuRole> pageTuts;
			pageTuts = this.menuRoleRepository.findByIsDelete(false, pagingSort);
			menuRole = pageTuts.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("variants", menuRole);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());

			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("menuRole/getMenuByMenuId/{id}")
	public ResponseEntity<List<MmenuRole>> checkMenuByMenuId(@PathVariable("id") Long id) {
		try {
			List<MmenuRole> menuMenu = this.menuRoleRepository.findByMenuIdAndIsDelete(id, false);
			return new ResponseEntity<List<MmenuRole>>(menuMenu, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
}
