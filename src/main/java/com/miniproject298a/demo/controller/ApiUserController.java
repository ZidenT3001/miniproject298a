package com.miniproject298a.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.model.Muser;
import com.miniproject298a.demo.repository.MuserRepository;
import com.miniproject298a.demo.model.TresetPassword;
import com.miniproject298a.demo.repository.ResetPasswordRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiUserController {

	@Autowired
	public MuserRepository muserRepository;

	@Autowired
	public ResetPasswordRepository resetPasswordRepository;

	@GetMapping("emailsearch/{email}")
	public ResponseEntity<Object> getSearchEmail(@PathVariable("email") String email) {

		try {
			Optional<Muser> dataUser = this.muserRepository.findEmailByKeyword(email);
			return new ResponseEntity<>(dataUser, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}

	// crosscheck email & password, locked if 3x wrong password

	@PostMapping("user/add")
	public ResponseEntity<Object> saveMuser(@RequestBody Muser muser) {
		String encryptedPassword = BCrypt.hashpw(muser.getPassword(), BCrypt.gensalt());
		muser.setPassword(encryptedPassword);
		muser.setCreatedOn(new Date());
		muser.setCreatedBy((long) 1);
		muser.setLoginAttempt(0);
		muser.setIsDelete(false);
		Muser userData = this.muserRepository.save(muser);
		if (userData.equals(muser)) {
			return new ResponseEntity<>(userData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("user/edit")
	public ResponseEntity<Object> editUser(@RequestBody Muser muser) {
		String email = muser.getEmail();
		String encryptedNewPassword = BCrypt.hashpw(muser.getPassword(), BCrypt.gensalt());

		Optional<Muser> dataUser = this.muserRepository.findEmailByKeyword(email);
		if (dataUser.isPresent()) {

			TresetPassword resetPassword = new TresetPassword();

			resetPassword.setCreatedBy(dataUser.get().getId());
			resetPassword.setNewPassword(encryptedNewPassword);
			resetPassword.setOldPassword(dataUser.get().getPassword());
			// resetPassword.setResetFor(email);
			resetPassword.setCreatedOn(new Date());
			resetPassword.setIsDelete(false);

			this.resetPasswordRepository.save(resetPassword);

			muser.setId(dataUser.get().getId());
			muser.setEmail(dataUser.get().getEmail());
			muser.setPassword(encryptedNewPassword);
			muser.setModifiedOn(dataUser.get().getModifiedOn());
			muser.setModifiedBy(dataUser.get().getModifiedBy());

			muser.setCreatedBy(dataUser.get().getCreatedBy());
			muser.setCreatedOn(dataUser.get().getCreatedOn());
			muser.setBiodataId(dataUser.get().getBiodataId());
			muser.setRoleId(dataUser.get().getRoleId());
			muser.setDeletedBy(dataUser.get().getDeletedBy());
			muser.setDeletedOn(dataUser.get().getDeletedOn());
			muser.setIsDelete(false);
			muser.setIsLocked(dataUser.get().getIsLocked());
			muser.setLastLogin(new Date());
			muser.setLoginAttempt(0);

			this.muserRepository.save(muser);

			return new ResponseEntity<>(dataUser, HttpStatus.OK);

		} else {
			return new ResponseEntity<>("error", HttpStatus.OK);
		}

	}

	@PutMapping("userlogin")
	public ResponseEntity<Object> userLoginAttempt(@RequestBody Muser muser) {
		String email = muser.getEmail();
		String plainPassword = muser.getPassword();

		Optional<Muser> dataUser = this.muserRepository.findEmailByKeyword(email);
		if (dataUser.isPresent()) {
			if (BCrypt.checkpw(plainPassword, dataUser.get().getPassword())) {

				// email dan password benar, ubah last login
				muser.setId(dataUser.get().getId());
				muser.setEmail(dataUser.get().getEmail());
				muser.setPassword(dataUser.get().getPassword());
				muser.setModifiedOn(dataUser.get().getModifiedOn());
				muser.setModifiedBy(dataUser.get().getModifiedBy());

				muser.setCreatedBy(dataUser.get().getCreatedBy());
				muser.setCreatedOn(dataUser.get().getCreatedOn());
				muser.setBiodataId(dataUser.get().getBiodataId());
				muser.setRoleId(dataUser.get().getRoleId());
				muser.setDeletedBy(dataUser.get().getDeletedBy());
				muser.setDeletedOn(dataUser.get().getDeletedOn());
				muser.setIsDelete(false);
				muser.setIsLocked(dataUser.get().getIsLocked());
				muser.setLastLogin(new Date());
				muser.setLoginAttempt(0);

				this.muserRepository.save(muser);

				return new ResponseEntity<>(dataUser, HttpStatus.OK);
			} else {

				// email benar dan password salah, ubah loginAttempt
				muser.setId(dataUser.get().getId());
				muser.setEmail(dataUser.get().getEmail());
				muser.setPassword(dataUser.get().getPassword());
				muser.setModifiedOn(dataUser.get().getModifiedOn());
				muser.setModifiedBy(dataUser.get().getModifiedBy());

				muser.setCreatedBy(dataUser.get().getCreatedBy());
				muser.setCreatedOn(dataUser.get().getCreatedOn());
				muser.setBiodataId(dataUser.get().getBiodataId());
				muser.setRoleId(dataUser.get().getRoleId());
				muser.setDeletedBy(dataUser.get().getDeletedBy());
				muser.setDeletedOn(dataUser.get().getDeletedOn());
				muser.setIsDelete(dataUser.get().getIsDelete());
				muser.setIsLocked(dataUser.get().getIsLocked());
				muser.setLastLogin(dataUser.get().getLastLogin());
				muser.setLoginAttempt((dataUser.get().getLoginAttempt()) + 1);

				this.muserRepository.save(muser);

				// jika percobaan login 3x salah password, akun terkunci
				if (muser.getLoginAttempt() == 3) {

					muser.setId(dataUser.get().getId());
					muser.setEmail(dataUser.get().getEmail());
					muser.setPassword(dataUser.get().getPassword());
					muser.setModifiedOn(dataUser.get().getModifiedOn());
					muser.setModifiedBy(dataUser.get().getModifiedBy());

					muser.setCreatedBy(dataUser.get().getCreatedBy());
					muser.setCreatedOn(dataUser.get().getCreatedOn());
					muser.setBiodataId(dataUser.get().getBiodataId());
					muser.setRoleId(dataUser.get().getRoleId());
					muser.setDeletedBy(dataUser.get().getDeletedBy());
					muser.setDeletedOn(dataUser.get().getDeletedOn());
					muser.setIsDelete(dataUser.get().getIsDelete());
					muser.setIsLocked(true);
					muser.setLastLogin(dataUser.get().getLastLogin());
					muser.setLoginAttempt(0);

					this.muserRepository.save(muser);

					return new ResponseEntity<>("account locked", HttpStatus.OK);
				}

				return new ResponseEntity<>("passwordsalah", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>("emailsalah", HttpStatus.OK);
		}

	}

	@GetMapping("getallmuser")
	public ResponseEntity<List<Muser>> getAllMuser() {
		try {
			List<Muser> listMuser = this.muserRepository.findAll();
			return new ResponseEntity<List<Muser>>(listMuser, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("get/muser/{id}")
	public ResponseEntity<List<Muser>> getMUserById(@PathVariable("id") long id) {
		try {
			Optional<Muser> mUserId = this.muserRepository.findById(id);
			System.out.println(mUserId);
			if (mUserId.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(mUserId, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Muser>>(HttpStatus.NO_CONTENT);
		}

	}

	@GetMapping("user/pagging")
	public ResponseEntity<Map<String, Object>> getAllHakAkses(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<Muser> mrole = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<Muser> pageTuts;
			pageTuts = this.muserRepository.findByIsDelete(false, pagingSort);
			mrole = pageTuts.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("variants", mrole);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());

			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
