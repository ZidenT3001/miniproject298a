package com.miniproject298a.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mbiodata;
import com.miniproject298a.demo.model.Mcustomer;
import com.miniproject298a.demo.model.McustomerMember;
import com.miniproject298a.demo.repository.ChatRepository;
import com.miniproject298a.demo.repository.McustomerMemberRepository;
import com.miniproject298a.demo.repository.McustomerRepository;
import com.miniproject298a.demo.repository.TappointmentDoneRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCustomerMemberController {

	@Autowired
	public McustomerMemberRepository customerMemberRepository;
	
	@Autowired
	public TappointmentDoneRepository tappointmentDoneRepository;

	@Autowired
	public ChatRepository chatHistoryRepository;

	@GetMapping("customermember")
	public ResponseEntity<List<McustomerMember>> getAllDataCustomer() {
		try {
			List<McustomerMember> listCustomer = this.customerMemberRepository.findByIsDelete(false);
			return new ResponseEntity<List<McustomerMember>>(listCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}

	@GetMapping("customermember/getByParentBiodata/{id}")
	public ResponseEntity<List<McustomerMember>> getAllDataPasien(@PathVariable("id") Long id) {
		try {
			List<McustomerMember> listPasien = this.customerMemberRepository.findByParentBiodataIdAndIsDeleted(id);
			return new ResponseEntity<List<McustomerMember>>(listPasien, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("customermember/{id}")
	public ResponseEntity<List<McustomerMember>> getDataCustomerById(@PathVariable("id") Long id) {
		try {
			Optional<McustomerMember> custmember = this.customerMemberRepository.findById(id);
			if (custmember.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(custmember, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("/search/custmember/{id}/{key}")
	public ResponseEntity<List<McustomerMember>> search(@PathVariable("id") Long id, @PathVariable("key") String key) {
		try {
			List<McustomerMember> searchBiodata = this.customerMemberRepository.searchByName(id, key);
			return new ResponseEntity<List<McustomerMember>>(searchBiodata, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("customermember/orderByAge/{id}")
	public ResponseEntity<List<McustomerMember>> sortByAge(@PathVariable("id") Long id) {
		try {
			List<McustomerMember> sortByAge = this.customerMemberRepository.sortByAge(id);
			return new ResponseEntity<List<McustomerMember>>(sortByAge, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("customermember/getByParentBiodataDesc/{id}")
	public ResponseEntity<List<McustomerMember>> getAllDataPasienDesc(@PathVariable("id") Long id) {
		try {
			List<McustomerMember> listPasien = this.customerMemberRepository.findByParentBiodataIdAndIsDeletedDesc(id);
			return new ResponseEntity<List<McustomerMember>>(listPasien, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("customermember/orderByAgeDesc/{id}")
	public ResponseEntity<List<McustomerMember>> sortByAgeDesc(@PathVariable("id") Long id) {
		try {
			List<McustomerMember> sortByAge = this.customerMemberRepository.sortByAgeDesc(id);
			return new ResponseEntity<List<McustomerMember>>(sortByAge, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	// Untuk menghitung banyak janji
	@GetMapping("appointmentDone/count/{idCustomer}")
	public ResponseEntity<Long> getJanjiPasien(@PathVariable("idCustomer") Long id) {
		try {
			Long janji = this.tappointmentDoneRepository.findHistoryJanji(id);
			return new ResponseEntity<Long>(janji, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

	// Untuk hitung banyak chat
	@GetMapping("chathistory/count/{idCustomer}")
	public ResponseEntity<Long> getChatHistoryPasien(@PathVariable("idCustomer") Long id) {
		try {
			Long chatHistory = this.chatHistoryRepository.findTotalChatHistory(id);
			return new ResponseEntity<Long>(chatHistory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

	// Untuk Paging
	// asc biasa
	@GetMapping("pagging/asc/{id}")
	public ResponseEntity<Map<String, Object>> getAllPasien(@PathVariable("id") Long id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		try {
			List<McustomerMember> pasien = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<McustomerMember> pageTuts;
			pageTuts = this.customerMemberRepository.sortByAsc(id, pagingSort);

			pasien = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("pasien", pasien);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("pagging/desc/{id}")
	public ResponseEntity<Map<String, Object>> getAllPasienDesc(@PathVariable("id") Long id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		try {
			List<McustomerMember> pasien = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<McustomerMember> pageTuts;
			pageTuts = this.customerMemberRepository.sortByDesc(id, pagingSort);

			pasien = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("pasien", pasien);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("pagging/age/asc/{id}")
	public ResponseEntity<Map<String, Object>> getAllAgePasien(@PathVariable("id") Long id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		try {
			List<McustomerMember> pasien = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<McustomerMember> pageTuts;
			pageTuts = this.customerMemberRepository.sortAgeAsc(id, pagingSort);

			pasien = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("pasien", pasien);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("pagging/age/desc/{id}")
	public ResponseEntity<Map<String, Object>> getAllAgePasienDesc(@PathVariable("id") Long id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		try {
			List<McustomerMember> pasien = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<McustomerMember> pageTuts;
			pageTuts = this.customerMemberRepository.sortAgeDesc(id, pagingSort);

			pasien = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("pasien", pasien);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
