package com.miniproject298a.demo.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.miniproject298a.demo.repository.HakAksesRepository;

@RestController
@RequestMapping("/hakakses/")
public class HakAksesController {
	
	@Autowired
	public HakAksesRepository hakAksesRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("hakakses/indexapi");
		return view;
	}


	

}
