package com.miniproject298a.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.MbloodGroup;
import com.miniproject298a.demo.model.Mcustomer;
import com.miniproject298a.demo.repository.MbloodGroupRepository;
import com.miniproject298a.demo.repository.McustomerRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiMbloodGroupController {

	@Autowired
	public  MbloodGroupRepository mbloodGroupRepository;
	
	@GetMapping("blood/list")
	public ResponseEntity<List<MbloodGroup>> getAllBlood() {
		try {
			List<MbloodGroup> listCustomer = this.mbloodGroupRepository.findByIsDelete(false);
			return new ResponseEntity<List<MbloodGroup>>(listCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
