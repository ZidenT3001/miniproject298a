package com.miniproject298a.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class landingPageController {

    @GetMapping("index")
    public String index() {
        return "initialPage.html";
    }

    @GetMapping("landingpage")
    public String landingpage() {
        return "landingpage.html";
    }

    @GetMapping("indexlogin")
    public String indexLogin() {
        return "indexlogin.html";
    }
    
    @GetMapping("profile")
    public String profile() {
    	return "profilelayout.html";
    }

}
