package com.miniproject298a.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mdoctor;
import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.repository.DokterRepository;
import com.miniproject298a.demo.repository.HakAksesRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiDokterController {
	
	@Autowired
	public  DokterRepository dokterRepository;
	
	@GetMapping("dokter")
	public ResponseEntity<List<Mdoctor>> getAllCategory() {
		try {
			List<Mdoctor> listCategory = this.dokterRepository.findByBiodataId();
			return new ResponseEntity<List<Mdoctor>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
