package com.miniproject298a.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mbiodata;
import com.miniproject298a.demo.model.Muser;
import com.miniproject298a.demo.repository.MbiodataRepository;
import com.miniproject298a.demo.repository.MuserRepository;
//
//@RestController
//@CrossOrigin("*")
//@RequestMapping("/api/registration/")
public class RegistrationController {
//
//	@Autowired
//	public MuserRepository muserRepository;
//	@Autowired
//	public MbiodataRepository mbiodataRepository;
//	@Autowired
//	private JavaMailSender javaMailSender;

//	@GetMapping("getallmuser")
//	public ResponseEntity<List<Muser>> getAllMuser() {
//		try {
//			List<Muser> listMuser = this.muserRepository.findAll();
//			return new ResponseEntity<List<Muser>>(listMuser, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		}
//	}

//	@GetMapping("getallmbiodata")
//	public ResponseEntity<List<Mbiodata>> getAllMbiodata() {
//		try {
//			List<Mbiodata> listMbiodata = this.mbiodataRepository.findAll();
//			return new ResponseEntity<List<Mbiodata>>(listMbiodata, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		}
//	}
//
//	@GetMapping("getmaxmbiodataid")
//	public ResponseEntity<Long> getMaxBiodataId() {
//		try {
//			Long maxId = this.mbiodataRepository.findByMaxId();
//			return new ResponseEntity<Long>(maxId, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
//		}
//	}

//	@GetMapping("emailsearch/{keyword}")
//	public ResponseEntity<Optional<Muser>> getSearchEmail(@PathVariable("keyword") String keyword) {
//		try {
//			Optional<Muser> dataUser = this.muserRepository.findEmailByKeyword(keyword);
//			return new ResponseEntity<>(dataUser, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		}
//	}

//	@GetMapping("passwodvalidation/{password}")
//	public ResponseEntity<Object> getValidate(@PathVariable("password") String password) {
//
//		char[] pass = password.toCharArray();
//
//		int hLeng = 0;
//		int hNum = 0;
//		int hLc = 0;
//		int hUc = 0;
//		int hSc = 0;
//
//		if (password.length() > 7) {
//			hLeng = 1;
//		}
//
//		for (int i = 0; i < pass.length; i++) {
//			if (pass[i] >= 48 && pass[i] <= 57) {
//				hNum = 1;
//				continue;
//			}
//			if (pass[i] >= 97 && pass[i] <= 122) {
//				hLc = 1;
//				continue;
//			}
//			if (pass[i] >= 65 && pass[i] <= 90) {
//				hUc = 1;
//				continue;
//			}
//			if ((pass[i] >= 33 && pass[i] <= 47) || (pass[i] >= 58 && pass[i] <= 64) || (pass[i] >= 97 && pass[i] <= 96)
//					|| (pass[i] >= 123 && pass[i] <= 126)) {
//				hSc = 1;
//				continue;
//			}
//		}
//
//		int val = hLeng + hNum + hLc + hUc + hSc;
//
//		if (val >= 5) {
//			return new ResponseEntity<>("strong", HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>("weak", HttpStatus.OK);
//		}
//
//	}

//	@PostMapping("biodata/add")
//	public ResponseEntity<Object> saveMbiodata(@RequestBody Mbiodata mbiodata) {
//		mbiodata.setCreatedOn(new Date());
//		Mbiodata userData = this.mbiodataRepository.save(mbiodata);
//		if (userData.equals(mbiodata)) {
//			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
//		}
//	}

//	@PostMapping("user/add")
//	public ResponseEntity<Object> saveMuser(@RequestBody Muser muser) {
//		String encryptedPassword = BCrypt.hashpw(muser.getPassword(), BCrypt.gensalt());
//		muser.setPassword(encryptedPassword);
//		muser.setCreatedOn(new Date());
//		muser.setLoginAttempt(0);
//		muser.setIsDelete(false);
//		Muser userData = this.muserRepository.save(muser);
//		if (userData.equals(muser)) {
//			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
//		}
//	}

//	@GetMapping("sendotp/{email}")
//	public ResponseEntity<Object> sendEmail(@PathVariable("email") String email) {
//
//		double angkaD = Math.random() * 1000000;
//		int angkaI = (int) angkaD;
//		String otp = angkaI + "";
//		while (otp.length() < 6) {
//			otp += "0";
//		}
//
//		SimpleMailMessage msg = new SimpleMailMessage();
//		msg.setTo(email);
//
//		msg.setSubject("Yout OTP For Med.id Registration is " + otp);
//		msg.setText("Hello, to proceed further with your registration to Med.id, please use the below OTP\n" + otp
//				+ "\nRegards,\n" + "Med.id Team");
//
//		javaMailSender.send(msg);
//		return new ResponseEntity<>(otp, HttpStatus.OK);
//
//	}

}
