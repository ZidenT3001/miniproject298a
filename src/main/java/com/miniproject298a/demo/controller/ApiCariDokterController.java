package com.miniproject298a.demo.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mdoctor;
import com.miniproject298a.demo.model.Mlocation;
import com.miniproject298a.demo.model.Mspecialization;
import com.miniproject298a.demo.model.TcurrentDoctorSpecialization;
import com.miniproject298a.demo.model.TdoctorOffice;
import com.miniproject298a.demo.model.TdoctorTreatment;
import com.miniproject298a.demo.repository.DokterRepository;
import com.miniproject298a.demo.repository.MSpecializationRepository;
import com.miniproject298a.demo.repository.MlocationLevelRepository;
import com.miniproject298a.demo.repository.MlocationRepository;
import com.miniproject298a.demo.repository.TCurrentDoctorSpecializationRepository;
import com.miniproject298a.demo.repository.TDoctorOfficeRepository;
import com.miniproject298a.demo.repository.TdoctorTreatmentRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/doctor/")
public class ApiCariDokterController {
	
	@Autowired
	private DokterRepository dokterRepository;
	
	@Autowired
	private MlocationLevelRepository mlocationLevelRepository;
	
	@Autowired 
	private MlocationRepository mlocationRepository;
	
	@Autowired
	private MSpecializationRepository mSpecializationRepository;
	
	@Autowired
	private TCurrentDoctorSpecializationRepository tCurrentDoctorSpecializationRepository;
	
	@Autowired
	private TDoctorOfficeRepository tDoctorOfficeRepository;
	
	@Autowired
	private TdoctorTreatmentRepository tdoctorTreatmentRepository;
	
	
	
	@GetMapping("carilokasi")
	public ResponseEntity<Object> getLokasi(){
		List<Mlocation> locationData = this.mlocationRepository.findAllByLevel();
		if (locationData.isEmpty()) {
			return new ResponseEntity<>("kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<>(locationData,HttpStatus.OK);
		}
	}
	
	@GetMapping("carispesial")
	public ResponseEntity<Object> getSpesial(){
		List<Mspecialization> spesialData = this.mSpecializationRepository.findAllByIsdelete();
		if (spesialData.isEmpty()) {
			return new ResponseEntity<>("kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<>(spesialData,HttpStatus.OK);
		}
	}
	
	@GetMapping("caridokterspesialis/{idspes}")
	public ResponseEntity<Object> getDokterspesial(@PathVariable("idspes") Long idspes){
		List<TcurrentDoctorSpecialization> dataSpesDokter = this.tCurrentDoctorSpecializationRepository.findBySpesialis(idspes);
		
		
		return new ResponseEntity<Object>(dataSpesDokter,HttpStatus.OK);
	}
	
	@GetMapping("caridoktername/{name}")
	public ResponseEntity<Object> getDokterName( @PathVariable("name") String name){
		List<Mdoctor> dataDokter = this.dokterRepository.findByNamaDok(name);
		
		if (dataDokter.isEmpty()) {
			return new ResponseEntity<Object>("kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(dataDokter,HttpStatus.OK);
		}
		
	}
	

	
	@GetMapping("caridokterlok/{idlok}")
	public ResponseEntity<Object> getDokterLok(@PathVariable("idlok") Long idlok, @PathVariable("iddok") Long iddok){
		
		List<TdoctorOffice> dataOffice = this.tDoctorOfficeRepository.findByLokDok(idlok,iddok);
		//List<> tes = [dataSpesDokter, dataNamaDokter, dataLokasi];
		if (dataOffice.isEmpty()) {
			return new ResponseEntity<Object>("tidak",HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("ada",HttpStatus.OK);
		}
		
	}
	
	@GetMapping("caridokteroffice/{iddok}")
	public ResponseEntity<Object> getDokterOffice(@PathVariable("iddok") Long iddok){
		
		List<TdoctorOffice> dataOffice = this.tDoctorOfficeRepository.findByIdDok(iddok);
		//List<> tes = [dataSpesDokter, dataNamaDokter, dataLokasi];
		
		return new ResponseEntity<Object>(dataOffice,HttpStatus.OK);
	}
	
	@GetMapping("caridokteroffice/all")
	public ResponseEntity<Object> getDokterOfficeAll(){
		
		List<TdoctorOffice> dataOffice = this.tDoctorOfficeRepository.findAll();
		//List<> tes = [dataSpesDokter, dataNamaDokter, dataLokasi];
		
		return new ResponseEntity<Object>(dataOffice,HttpStatus.OK);
	}
	
	@GetMapping("caritindakan")
	public ResponseEntity<Object> getTindakan(){
		List<TdoctorTreatment> tindakanData = this.tdoctorTreatmentRepository.findAllByIsdelete();
		if (tindakanData.isEmpty()) {
			return new ResponseEntity<>("kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<>(tindakanData,HttpStatus.OK);
		}
	}
	
	@GetMapping("caridokterlok/{idlok}/{iddok}")
	public ResponseEntity<Object> getDokterLok2(@PathVariable("idlok") Long idlok, @PathVariable("iddok") Long iddok){
		
		List<TdoctorOffice> dataOffice = this.tDoctorOfficeRepository.findByLokDok(idlok,iddok);
		//List<> tes = [dataSpesDokter, dataNamaDokter, dataLokasi];
		if (dataOffice.isEmpty()) {
			return new ResponseEntity<Object>("tidak",HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("ada",HttpStatus.OK);
		}
		
	}
	
	@GetMapping("caridoktertindakan/{iddok}")
	public ResponseEntity<Object> getDokterTindak(@PathVariable("iddok") Long iddok){
		
//		List<TDoctorOfficeTreatment> dataOffice = this.tDoctorOfficeTreatmentRepository.findTindakan(iddok,tindakan);
		List<TdoctorTreatment> dataOffice = this.tdoctorTreatmentRepository.findTindakanByIdDok(iddok);
		if (dataOffice.isEmpty()) {
			return new ResponseEntity<Object>("tidak",HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(dataOffice,HttpStatus.OK);
		}
		
	}
}
