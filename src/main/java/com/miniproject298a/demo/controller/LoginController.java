package com.miniproject298a.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Muser;
import com.miniproject298a.demo.repository.MbiodataRepository;
import com.miniproject298a.demo.repository.MuserRepository;
//
//@RestController
//@CrossOrigin("*")
//@RequestMapping("/api/login/")
public class LoginController {
//
//	@Autowired
//	public MuserRepository muserRepository;
//	@Autowired
//	public MbiodataRepository mbiodataRepository;

	// crosscheck email & password
//	@PostMapping("userlogin")
//	public ResponseEntity<Object> userLogin(@RequestBody Muser muser) {
//		String email = muser.getEmail();
//		String plainPassword = muser.getPassword();
//
//		Optional<Muser> dataUser = this.muserRepository.findEmailByKeyword(email);
//		if (dataUser.isPresent()) {
//			if (BCrypt.checkpw(plainPassword, dataUser.get().getPassword())) {
//				return new ResponseEntity<>(dataUser, HttpStatus.OK);
//			} else {
//				return new ResponseEntity<>("passwordsalah", HttpStatus.OK);
//			}
//		} else {
//			return new ResponseEntity<>("emailsalah", HttpStatus.OK);
//		}
//
//	}
//	
//	@GetMapping("userlogin/{email}")
//	public ResponseEntity<Object> GetuserLoginInfo(@PathVariable("email") String email) {
//		
//		try {
//			Optional<Muser> dataUser = this.muserRepository.findEmailByKeyword(email);
//			return new ResponseEntity<>(dataUser, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		}
//
//	}
	
	

//	// crosscheck email & password, locked if 3x wrong password
//	@PutMapping("userlogin")
//	public ResponseEntity<Object> userLoginAttempt(@RequestBody Muser muser) {
//		String email = muser.getEmail();
//		String plainPassword = muser.getPassword();
//
//		Optional<Muser> dataUser = this.muserRepository.findEmailByKeyword(email);
//		if (dataUser.isPresent()) {
//			if (BCrypt.checkpw(plainPassword, dataUser.get().getPassword())) {
//
//				// email dan password benar, ubah last login
//				muser.setId(dataUser.get().getId());
//				muser.setEmail(dataUser.get().getEmail());
//				muser.setPassword(dataUser.get().getPassword());
//				muser.setModifiedOn(dataUser.get().getModifiedOn());
//				muser.setModifiedBy(dataUser.get().getModifiedBy());
//
//				muser.setCreatedBy(dataUser.get().getCreatedBy());
//				muser.setCreatedOn(dataUser.get().getCreatedOn());
//				muser.setBiodataId(dataUser.get().getBiodataId());
//				muser.setRoleId(dataUser.get().getRoleId());
//				muser.setDeletedBy(dataUser.get().getDeletedBy());
//				muser.setDeletedOn(dataUser.get().getDeletedOn());
//				muser.setIsDelete(false);
//				muser.setIsLocked(dataUser.get().getIsLocked());
//				muser.setLastLogin(new Date());
//				muser.setLoginAttempt(0);
//
//				this.muserRepository.save(muser);
//
//				return new ResponseEntity<>(dataUser, HttpStatus.OK);
//			} else {
//
//				// email benar dan password salah, ubah loginAttempt
//				muser.setId(dataUser.get().getId());
//				muser.setEmail(dataUser.get().getEmail());
//				muser.setPassword(dataUser.get().getPassword());
//				muser.setModifiedOn(dataUser.get().getModifiedOn());
//				muser.setModifiedBy(dataUser.get().getModifiedBy());
//
//				muser.setCreatedBy(dataUser.get().getCreatedBy());
//				muser.setCreatedOn(dataUser.get().getCreatedOn());
//				muser.setBiodataId(dataUser.get().getBiodataId());
//				muser.setRoleId(dataUser.get().getRoleId());
//				muser.setDeletedBy(dataUser.get().getDeletedBy());
//				muser.setDeletedOn(dataUser.get().getDeletedOn());
//				muser.setIsDelete(dataUser.get().getIsDelete());
//				muser.setIsLocked(dataUser.get().getIsLocked());
//				muser.setLastLogin(dataUser.get().getLastLogin());
//				muser.setLoginAttempt((dataUser.get().getLoginAttempt()) + 1);
//
//				this.muserRepository.save(muser);
//
//				// jika percobaan login 3x salah password, akun terkunci
//				if (muser.getLoginAttempt() == 3) {
//					
//					muser.setId(dataUser.get().getId());
//					muser.setEmail(dataUser.get().getEmail());
//					muser.setPassword(dataUser.get().getPassword());
//					muser.setModifiedOn(dataUser.get().getModifiedOn());
//					muser.setModifiedBy(dataUser.get().getModifiedBy());
//
//					muser.setCreatedBy(dataUser.get().getCreatedBy());
//					muser.setCreatedOn(dataUser.get().getCreatedOn());
//					muser.setBiodataId(dataUser.get().getBiodataId());
//					muser.setRoleId(dataUser.get().getRoleId());
//					muser.setDeletedBy(dataUser.get().getDeletedBy());
//					muser.setDeletedOn(dataUser.get().getDeletedOn());
//					muser.setIsDelete(dataUser.get().getIsDelete());
//					muser.setIsLocked(true);
//					muser.setLastLogin(dataUser.get().getLastLogin());
//					muser.setLoginAttempt(0);
//					
//					this.muserRepository.save(muser);
//					
//				}
//
//				return new ResponseEntity<>("passwordsalah", HttpStatus.OK);
//			}
//		} else {
//			return new ResponseEntity<>("emailsalah", HttpStatus.OK);
//		}
//
//	}

	// ubah last login
//		@PutMapping("userlogin/{id}")
//		public ResponseEntity<Object> editUserLogin(@PathVariable("id") Long id, @RequestBody Muser muser) {
//			Optional<Muser> userData = this.muserRepository.findById(id);
//			if (userData.isPresent()) {
//				muser.setId(userData.get().getId());
//				muser.setEmail(userData.get().getEmail());
//				muser.setPassword(userData.get().getPassword());
//				muser.setModifiedOn(userData.get().getModifiedOn());
//				muser.setModifiedBy(userData.get().getModifiedBy());
//
//				muser.setCreatedBy(userData.get().getCreatedBy());
//				muser.setCreatedOn(userData.get().getCreatedOn());
//				muser.setBiodataId(userData.get().getBiodataId());
//				muser.setRoleId(userData.get().getRoleId());
//				muser.setDeletedBy(userData.get().getDeletedBy());
//				muser.setDeletedOn(userData.get().getDeletedOn());
//				muser.setIsDelete(userData.get().getIsDelete());
//				muser.setIsLocked(userData.get().getIsLocked());
//				muser.setLastLogin(new Date());
//				muser.setLoginAttempt(0);
//
//				this.muserRepository.save(muser);
//				return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
//			} else {
//				return ResponseEntity.notFound().build();
//			}
//		}

}
