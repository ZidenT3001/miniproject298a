package com.miniproject298a.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mmenu;
import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.repository.AturAksesMenuRepository;
import com.miniproject298a.demo.repository.HakAksesRepository;

@RestController
@RequestMapping("/api/")
public class ApiAturAksesMenuController {
	
	@Autowired
	public AturAksesMenuRepository aturAksesMenuRepository;
	
	@GetMapping("menu")
	public List<Mmenu> getall(){
		
		return aturAksesMenuRepository.findByIsDelete(false);
	}
	
	@GetMapping("/menu/{id}")
	public Optional<Mmenu> findById(@PathVariable("id") Long id){
		return aturAksesMenuRepository.findById(id);
	}
	
	@PutMapping("menu/edit/{id}")
	public ResponseEntity<Object> editMenu(@PathVariable("id") Long id, @RequestBody Mmenu mmenu) {
		Optional<Mmenu> menuData = this.aturAksesMenuRepository.findById(id);
		if (menuData.isPresent()) {
			mmenu.setId(id);		
			//mmenu.setParent(mmenu);
			mmenu.setModifiedBy(id);
			mmenu.setModifiedOn(Date.from(Instant.now()));
			mmenu.setCreatedBy(menuData.get().getCreatedBy());
			mmenu.setCreatedOn(menuData.get().getCreatedOn());
			this.aturAksesMenuRepository.save(mmenu);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	
}
