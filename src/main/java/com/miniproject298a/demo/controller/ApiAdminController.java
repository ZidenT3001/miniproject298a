package com.miniproject298a.demo.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Madmin;
import com.miniproject298a.demo.repository.MadminRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiAdminController {
	
	@Autowired
	public MadminRepository madminRepository;
	
	@PostMapping("admin/add")
	public ResponseEntity<Object> saveAdmin(@RequestBody Madmin madmin){
		madmin.setCreatedOn(new Date());
		madmin.setIsDelete(false);
		Madmin madminData = this.madminRepository.save(madmin);
		if(madminData.equals(madmin)) {
			return new ResponseEntity<>("save data successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
		}
	}

}
