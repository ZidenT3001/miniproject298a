package com.miniproject298a.demo.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject298a.demo.model.Mrole;
import com.miniproject298a.demo.repository.HakAksesRepository;
import com.miniproject298a.demo.repository.MbiodataRepository;
import com.miniproject298a.demo.repository.MuserRepository;

@RestController
@RequestMapping("/api/")
public class ApiHakAksesController {
	
	@Autowired
	public HakAksesRepository hakAksesRepository;
	
	@Autowired
	public MbiodataRepository mbiodataRepository;
	
	@Autowired
	public MuserRepository MuserRepository;
	
	@GetMapping("hakakses")
	public ResponseEntity<List<Mrole>> getAllCategory() {
		try {
			List<Mrole> listCategory = this.hakAksesRepository.findByIsDelete(false);
			return new ResponseEntity<List<Mrole>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("hakakses/{id}")
	public ResponseEntity<List<Mrole>> getHakAksesById(@PathVariable("id") Long id){
		try {
			Optional<Mrole> mrole = this.hakAksesRepository.findById(id);
			if (mrole.isPresent()) {
				ResponseEntity rest = new ResponseEntity(mrole, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Mrole>>(HttpStatus.NO_CONTENT);
		}
	}
	
//	@GetMapping("cariNameUser")
//	public ResponseEntity<List<Mrole>> getNameUser(){
//		try {
//			List<Mrole> namaUser = this.hakAksesRepository.findNamaByUserAndBiodata();
//			return new ResponseEntity<List<Mrole>>(namaUser, HttpStatus.OK);
//		} catch(Exception e) {
//			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		}
//	}
	
	
	@PostMapping("hakakses/add")
	public ResponseEntity<Object> saveHakAkses(@RequestBody Mrole role) {
		
		String name = role.getName();
		String code = role.getCode();
		
		Optional<Mrole> roleData = this.hakAksesRepository.findByNameCodeAndIsdeledte(name, code, false);
		
		if(roleData.isEmpty()) {
			role.setCreatedBy(1L);
			role.setCreatedOn(new Date());
			role.setIsDelete(false);
			Mrole mRoleData = this.hakAksesRepository.save(role);
			if (mRoleData.equals(role)) {
				return new ResponseEntity<>("Success", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<>("Data Yang Anda Input Sudah Ada ! ", HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@PutMapping("edit/hakakses/{id}")
	public ResponseEntity<Object> editHakAkses(@PathVariable("id") Long id, @RequestBody Mrole mrole) {
		
		Optional<Mrole> mroleData = this.hakAksesRepository.findById(id);
		
		if (mroleData.isPresent()) {
			mrole.setId(id);
			mrole.setModifiedBy(mroleData.get().getCreatedBy());
			mrole.setModifiedOn(new Date());
			mrole.setCreatedBy(mroleData.get().getCreatedBy());
			mrole.setIsDelete(false);
			mrole.setCreatedOn(mroleData.get().getCreatedOn());
			mrole.setCreatedBy(mroleData.get().getCreatedBy());
			this.hakAksesRepository.save(mrole);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}


	// dalete category
	@PutMapping("delete/hakakses/{id}")
	public ResponseEntity<Object> deleteHakAkses(@PathVariable("id") Long id) {
		
		Optional<Mrole> mroleData = this.hakAksesRepository.findById(id);
		
		if (mroleData.isPresent()) {
			Mrole mrole = new Mrole();
			mrole.setId(id);
			mrole.setIsDelete(true);
			mrole.setDeletedBy(mroleData.get().getCreatedBy());
			mrole.setDeletedOn(new Date());
			mrole.setCreatedBy(mroleData.get().getCreatedBy());
			mrole.setCreatedOn(mroleData.get().getCreatedOn());
			mrole.setCode(mroleData.get().getCode());
			mrole.setName(mroleData.get().getName());
			mrole.setModifiedBy(mroleData.get().getModifiedBy());
			mrole.setModifiedOn(mroleData.get().getModifiedOn());
			
			
			this.hakAksesRepository.save(mrole);
			return new ResponseEntity<>("Deleted Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("hakakses/pagging")
	public ResponseEntity<Map<String, Object>> getAllHakAkses(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size){
		try {
			List<Mrole> mrole = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<Mrole> pageTuts;
			pageTuts = this.hakAksesRepository.findByIsDelete(false, pagingSort);
			mrole = pageTuts.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("variants", mrole);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
						
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("hakaksesDesc/pagging")
	public ResponseEntity<Map<String, Object>> getAllHakAksesDesc(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size){
		try {
			List<Mrole> mrole = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<Mrole> pageTuts;
			pageTuts = this.hakAksesRepository.findByIsDeleteDesc(false, pagingSort);
			mrole = pageTuts.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("variants", mrole);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
						
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("cariHakAkses/{key}")
	public ResponseEntity<List<Mrole>> searchCategory(@PathVariable("key") String key) {
		try {
			List<Mrole> listCategory = this.hakAksesRepository.searchByKey(key);
			return new ResponseEntity<List<Mrole>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
