package com.miniproject298a.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class DoctorProfileLayoutController {

	 @GetMapping("doctor")
	    public String doctor() {
	        return "doctorprofilelayout/doctorprofile.html";
	    }
	 @GetMapping("spesialisasi")
	 public String spesialisasi() {
		 return "doctorprofilelayout/spesialisasiDoctor.html";
	 }
	 
}
