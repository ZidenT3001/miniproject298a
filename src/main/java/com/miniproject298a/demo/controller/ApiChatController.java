package com.miniproject298a.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mcustomer;
import com.miniproject298a.demo.repository.ChatRepository;
import com.miniproject298a.demo.repository.McustomerRepository;


@RestController
@RequestMapping("/api/")
public class ApiChatController {
	@Autowired
	public ChatRepository chatRepository;
	
	@GetMapping("chat/{id}")
	public ResponseEntity<Long> getAllcustomer(@PathVariable("id") Long id) {
		try {
			Long chat = this.chatRepository.findByChat(id);
			return new ResponseEntity<Long>(chat, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
