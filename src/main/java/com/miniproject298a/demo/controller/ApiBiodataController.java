package com.miniproject298a.demo.controller;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.miniproject298a.demo.model.Mbiodata;
import com.miniproject298a.demo.repository.MbiodataRepository;
import com.miniproject298a.demo.service.FileStorageService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiBiodataController {

	@Autowired
	public MbiodataRepository mbiodataRepository;
	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	FileStorageService fileStorage;

	private final Path root = Paths.get("src/main/resources/static/img");
	private static final List<String> contentTypes = Arrays.asList("image/png", "image/jpeg", "image/jpg");
	private Long maxFileSize = 1024 * 1024 * 2L; // 1024*1024* 2byte = 2MB // max 10mb

	@GetMapping("getallmbiodata")
	public ResponseEntity<List<Mbiodata>> getAllMbiodata() {
		try {
			List<Mbiodata> listMbiodata = this.mbiodataRepository.findAll();
			return new ResponseEntity<List<Mbiodata>>(listMbiodata, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("getmaxmbiodataid")
	public ResponseEntity<Long> getMaxBiodataId() {
		try {
			Long maxId = this.mbiodataRepository.findByMaxId();
			return new ResponseEntity<Long>(maxId, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("biodata/add")
	public ResponseEntity<Object> saveMbiodata(@RequestBody Mbiodata mbiodata) {
		mbiodata.setCreatedOn(new Date());
		mbiodata.setIsDelete(false);
		Mbiodata userData = this.mbiodataRepository.save(mbiodata);
		if (userData.equals(mbiodata)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("passwordvalidation/{password}")
	public ResponseEntity<Object> getValidate(@PathVariable("password") String password) {

		char[] pass = password.toCharArray();

		int hLeng = 0;
		int hNum = 0;
		int hLc = 0;
		int hUc = 0;
		int hSc = 0;

		if (password.length() > 7) {
			hLeng = 1;
		}

		for (int i = 0; i < pass.length; i++) {
			if (pass[i] >= 48 && pass[i] <= 57) {
				hNum = 1;
				continue;
			}
			if (pass[i] >= 97 && pass[i] <= 122) {
				hLc = 1;
				continue;
			}
			if (pass[i] >= 65 && pass[i] <= 90) {
				hUc = 1;
				continue;
			}
			if ((pass[i] >= 33 && pass[i] <= 47) || (pass[i] >= 58 && pass[i] <= 64) || (pass[i] >= 97 && pass[i] <= 96)
					|| (pass[i] >= 123 && pass[i] <= 126)) {
				hSc = 1;
				continue;
			}
		}

		int val = hLeng + hNum + hLc + hUc + hSc;

		if (val >= 5) {
			return new ResponseEntity<>("strong", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("weak", HttpStatus.OK);
		}

	}

	@GetMapping("sendotp/{email}")
	public ResponseEntity<Object> sendEmail(@PathVariable("email") String email) {

		double angkaD = Math.random() * 1000000;
		int angkaI = (int) angkaD;
		String otp = angkaI + "";
		while (otp.length() < 6) {
			otp += "0";
		}

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(email);

		msg.setSubject("Yout OTP For Med.id Registration is " + otp);
		msg.setText("Hello, to proceed further with your registration to Med.id, please use the below OTP\n" + otp
				+ "\nRegards,\n" + "Med.id Team");

		//javaMailSender.send(msg);
		return new ResponseEntity<>(otp, HttpStatus.OK);

	}

	@PostMapping("BiodataPasien/add")
	public ResponseEntity<Object> saveBiodataPasien(@RequestBody Mbiodata mbiodata) {
		String name = mbiodata.getFullName();
		Long createBy = mbiodata.getCreatedBy();

		Optional<Mbiodata> biodataData = this.mbiodataRepository.findByNameCreateByAndIsDelete(name, createBy, false);

		if (biodataData.isEmpty()) {
			mbiodata.setCreatedOn(new Date());
			mbiodata.setIsDelete(false);

			Mbiodata bioData = this.mbiodataRepository.save(mbiodata);
			if (bioData.equals(mbiodata)) {
				Long maxid = bioData.getId();
				return new ResponseEntity<>(maxid, HttpStatus.OK);
			} else {
				return new ResponseEntity<>("failed", HttpStatus.OK);
			}

		} else {
			return new ResponseEntity<>("failed", HttpStatus.OK);
		}

	}

	@PutMapping("edit/biodata/{id}")
	public ResponseEntity<Object> editBiodataById(@PathVariable("id") long id, @RequestBody Mbiodata mBiodata) {
		Optional<Mbiodata> mBiodataData = this.mbiodataRepository.findById(id);

		if (mBiodataData.isPresent()) {
			mBiodata.setId(id);
			mBiodata.setCreatedBy(mBiodataData.get().getCreatedBy());
			mBiodata.setCreatedOn(mBiodataData.get().getCreatedOn());
			mBiodata.setIsDelete(mBiodataData.get().getIsDelete());
			mBiodata.setModifiedOn(Date.from(Instant.now()));
			mBiodata.setModifiedBy(mBiodataData.get().getCreatedBy());
			this.mbiodataRepository.save(mBiodata);
			return new ResponseEntity<Object>("Upload Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}

	}

	@GetMapping("biodata/{id}")
	public ResponseEntity<List<Mbiodata>> getBiodataById(@PathVariable("id") Long id) {
		try {
			Optional<Mbiodata> biodataPasien = this.mbiodataRepository.findById(id);
			if (biodataPasien.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(biodataPasien, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Mbiodata>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("biodataPasien/delete/{id}")
	public ResponseEntity<Object> deleteBiodataById(@PathVariable("id") Long id) {
		Optional<Mbiodata> dataBio = this.mbiodataRepository.findById(id);

		if (dataBio.isPresent()) {
			Mbiodata bio = new Mbiodata();

			bio.setId(id);
			bio.setFullName(dataBio.get().getFullName());
			bio.setCreatedBy(dataBio.get().getCreatedBy());
			bio.setCreatedOn(dataBio.get().getCreatedOn());
			bio.setDeletedBy(dataBio.get().getCreatedBy());
			bio.setDeletedOn(new Date());
			bio.setModifiedOn(dataBio.get().getModifiedOn());
			bio.setModifiedBy(dataBio.get().getModifiedBy());
			bio.setIsDelete(true);
			this.mbiodataRepository.save(bio);
			return new ResponseEntity<>("Deleted Successfuly", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	//image
	@PutMapping("biodata/image/{idUserLogin}/{id}")
	public ResponseEntity<Object> uploadImageBiodata(@PathVariable("idUserLogin") Long idUserLogin,
			@PathVariable("id") Long id, @RequestParam("file") MultipartFile file) {

		String pathImage = "/api/files/" + file.getOriginalFilename();
		Long sizeOfFile = file.getSize();
		String typeOfFile = file.getContentType();

		System.out.println("file name: " + file.getOriginalFilename());
		System.out.println("byte: " + file.getSize());
		System.out.println("path: " + root + "/" + file.getOriginalFilename());
		System.out.println("type: " + file.getContentType());

		// handling bukan tipe gambar
		if (!contentTypes.contains(typeOfFile)) {
			System.out.println(
					"Bukan file gambar. Silahkan upload hanya file gambar yang memiliki ekstensi .png, .jpg, atau .jpeg");
			return new ResponseEntity<>("Bukan file gambar", HttpStatus.BAD_REQUEST);
		}

		// handling ukuran gambar
		if (sizeOfFile > maxFileSize) {
			System.out.println("Ukuran file terlalu besar. Maksimal sama dengan 2MB");
			return new ResponseEntity<>("Ukuran file terlalu besar", HttpStatus.BAD_REQUEST);
		}

		// simpan gambar ke folder root/upload
		this.fileStorage.save(file);

		// update ke database
		Optional<Mbiodata> mBiodataData = this.mbiodataRepository.findById(id);
		if (mBiodataData.isPresent()) {
			Mbiodata mBiodata = mBiodataData.get();
			mBiodata.setId(id);
			mBiodata.setModifiedBy(idUserLogin);
			mBiodata.setModifiedOn(new Date());
			mBiodata.setImagePath(pathImage);
			this.mbiodataRepository.save(mBiodata);
			return new ResponseEntity<>("Operation successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//image
	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Object> getFile(@PathVariable String filename) {
		Resource file = this.fileStorage.load(filename);
		if (file == null) {
			return new ResponseEntity<>("Ukuran file terlalu besar", HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

}
