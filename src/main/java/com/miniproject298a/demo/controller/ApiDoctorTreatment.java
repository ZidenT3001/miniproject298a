package com.miniproject298a.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.Mlocation;
import com.miniproject298a.demo.model.TdoctorTreatment;
import com.miniproject298a.demo.repository.TDoctorOfficeRepository;
import com.miniproject298a.demo.repository.TdoctorTreatmentRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiDoctorTreatment {
	
	@Autowired
	private  TdoctorTreatmentRepository tdoctorTreatmentRepository;
	
	@GetMapping("treatment")
	public ResponseEntity<Object> getLokasi(){
		List<TdoctorTreatment> locationData = this.tdoctorTreatmentRepository.findAll();
		if (locationData.isEmpty()) {
			return new ResponseEntity<>("kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<>(locationData,HttpStatus.OK);
		}
	}

}
