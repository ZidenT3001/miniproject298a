package com.miniproject298a.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298a.demo.model.MbiodataAddress;
import com.miniproject298a.demo.model.Mlocation;
import com.miniproject298a.demo.model.MlocationLevel;
import com.miniproject298a.demo.model.MmedicalFacility;
import com.miniproject298a.demo.repository.MbiodataAddressRepository;
import com.miniproject298a.demo.repository.MlocationLevelRepository;
import com.miniproject298a.demo.repository.MlocationRepository;
import com.miniproject298a.demo.repository.MmedicalFacilityRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiLocationController {

	@Autowired
	private MlocationRepository mlocationRepository;

	@Autowired
	private MlocationLevelRepository mlocationLevelRepository;

	@Autowired
	private MbiodataAddressRepository mbiodataAddressRepository;

	@Autowired
	private MmedicalFacilityRepository mmedicalFacilityRepository;

	@GetMapping("lokasi")
	public ResponseEntity<List<Mlocation>> getAllLocation() {
		try {
			List<Mlocation> listLocation = this.mlocationRepository.findByIsDelete(false);
			return new ResponseEntity<List<Mlocation>>(listLocation, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("lokasi/{id}")
	public ResponseEntity<List<Mlocation>> getMlocationById(@PathVariable("id") Long id) {
		try {
			Optional<Mlocation> mlocation = this.mlocationRepository.findById(id);
			if (mlocation.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(mlocation, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Mlocation>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("lokasi/level/{level}")
	public ResponseEntity<List<Mlocation>> getAllLocationByLevel(@PathVariable("level") int level) {
		try {
			List<Mlocation> listLocation = this.mlocationRepository.findByIsDeleteAndLevel(level);
			return new ResponseEntity<List<Mlocation>>(listLocation, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("levellokasi")
	public ResponseEntity<List<MlocationLevel>> getAllLevel() {
		try {
			List<MlocationLevel> listLevelLocation = this.mlocationLevelRepository.findByIsDelete(false);
			return new ResponseEntity<List<MlocationLevel>>(listLevelLocation, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("lokasi/add")
	public ResponseEntity<Object> saveLokasi(@RequestBody Mlocation mlocation) {
		// mlocation.setCreatedBy();
		mlocation.setCreatedOn(new Date());
		mlocation.setIsDelete(false);
		Mlocation mlocationData = this.mlocationRepository.save(mlocation);

		if (mlocationData.equals(mlocation)) {
			return new ResponseEntity<>("save data successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("save data failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("edit/lokasi/{id}")
	public ResponseEntity<Object> editLokasi(@PathVariable("id") Long id, @RequestBody Mlocation mlocation) {
		Optional<Mlocation> mlocationData = this.mlocationRepository.findById(id);
		if (mlocationData.isPresent()) {
			mlocation.setId(id);

			mlocation.setCreatedBy(mlocationData.get().getCreatedBy());
			mlocation.setCreatedOn(mlocationData.get().getCreatedOn());
			mlocation.setModifyBy(id);
			mlocation.setModifiedOn(new Date());
			mlocation.setIsDelete(false);
			this.mlocationRepository.save(mlocation);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// delete lokasi
	@PutMapping("delete/lokasi/{id}")
	public ResponseEntity<Object> deleteLokasi(@PathVariable("id") Long id, @RequestBody Mlocation mlocation) {

		Optional<Mlocation> mlocationData = this.mlocationRepository.findById(id);

		if (mlocationData.isPresent()) {
			List<Mlocation> locationDataChild = this.mlocationRepository.findByParentId(id);
			List<MbiodataAddress> biodataValidation = this.mbiodataAddressRepository.findLocationById(id);
			List<MmedicalFacility> medicalFacilityValidation = this.mmedicalFacilityRepository.findLocationById(id);

			if (locationDataChild.isEmpty() && biodataValidation.isEmpty() && medicalFacilityValidation.isEmpty()) {
				mlocation.setId(id);
				mlocation.setName(mlocationData.get().getName());
				mlocation.setLocationLevelId(mlocationData.get().getLocationLevelId());
				mlocation.setParentId(mlocationData.get().getParentId());

				mlocation.setIsDelete(true);
				mlocation.setDeletedOn(new Date());
				mlocation.setCreatedBy(mlocationData.get().getCreatedBy());
				mlocation.setCreatedOn(mlocationData.get().getCreatedOn());
				mlocation.setModifyBy(mlocationData.get().getModifyBy());
				mlocation.setModifiedOn(mlocationData.get().getModifiedOn());

				this.mlocationRepository.save(mlocation);
				return new ResponseEntity<Object>("Data Berhasil Dihapus", HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>("Data Gagal Dihapus Karena Masih Digunakan", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<Object>("Error", HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("lokasi/pagging")
	public ResponseEntity<Map<String, Object>> getAllLokasi(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "0") int level) {
		try {
			List<Mlocation> mlocation = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<Mlocation> pageTuts;

			if (level == 0) {
				pageTuts = this.mlocationRepository.findByIsDelete(false, pagingSort);
				mlocation = pageTuts.getContent();
				Map<String, Object> response = new HashMap<>();
				response.put("variants", mlocation);
				response.put("currentPage", pageTuts.getNumber());
				response.put("totalItems", pageTuts.getTotalElements());
				response.put("totalPage", pageTuts.getTotalPages());
				return new ResponseEntity<>(response, HttpStatus.OK);
			} else {
				pageTuts = this.mlocationRepository.findByIsDeleteAndLevelPageable(level, pagingSort);
				mlocation = pageTuts.getContent();
				Map<String, Object> response = new HashMap<>();
				response.put("variants", mlocation);
				response.put("currentPage", pageTuts.getNumber());
				response.put("totalItems", pageTuts.getTotalElements());
				response.put("totalPage", pageTuts.getTotalPages());
				return new ResponseEntity<>(response, HttpStatus.OK);
			}

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("lokasi/searching")
	public ResponseEntity<List<Mlocation>> searchCategory(@RequestParam(defaultValue = "") String keyword,
			@RequestParam(defaultValue = "0") int level) {
		try {
			if (level == 0) {
				List<Mlocation> listLocation = this.mlocationRepository.searchByKey(keyword);
				return new ResponseEntity<List<Mlocation>>(listLocation, HttpStatus.OK);
			} else {
				List<Mlocation> listLocation = this.mlocationRepository.searchByKeyword(keyword, level);
				return new ResponseEntity<List<Mlocation>>(listLocation, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("lokasi/caridokterlokasi/{name}")
	public ResponseEntity<List<Mlocation>> getDokterLokasiFas(@PathVariable("name") String name) {
		List<Mlocation> dataDokter = this.mlocationRepository.findByNamaLokasiDok(name);

		if (dataDokter.isEmpty()) {
			return new ResponseEntity<List<Mlocation>>(dataDokter, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<Mlocation>>(dataDokter, HttpStatus.OK);
		}
	}

}
